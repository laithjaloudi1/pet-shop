<!doctype html>
<html class="no-js" lang="en">
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>The PetBite</title>
    <meta name="robots" content="noindex, follow" />
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('website/img/petlogo.png')}}">
    <!-- All CSS is here
    ============================================ -->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('website/assets/css/vendor/bootstrap.min.css')}}">
    <!-- Google font CSS -->
    <link rel="stylesheet" href="{{asset('website/assets/css/vendor/signpainter-housescript.css')}}">
    <!-- Icon Font CSS -->
    <link rel="stylesheet" href="{{asset('website/assets/css/vendor/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('website/assets/css/vendor/linearicons.css')}}">
    <link rel="stylesheet" href="{{asset('website/assets/css/vendor/themify-icons.css')}}">
    <!-- Others CSS -->
    <link rel="stylesheet" href="{{asset('website/assets/css/plugins/owl-carousel.css')}}">
    <link rel="stylesheet" href="{{asset('website/assets/css/plugins/slick.css')}}">
    <link rel="stylesheet" href="{{asset('website/assets/css/plugins/animate.css')}}">
    <link rel="stylesheet" href="{{asset('website/assets/css/plugins/jquery.mb.ytplayer.min.css')}}">
    <link rel="stylesheet" href="{{asset('website/assets/css/plugins/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('website/assets/css/plugins/jarallax.css')}}">
    <link rel="stylesheet" href="{{asset('website/assets/css/plugins/jquery-ui.css')}}">
    <link rel="stylesheet" href="{{asset('website/assets/css/plugins/easyzoom.css')}}">
    <link rel="stylesheet" href="{{asset('website/assets/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href={{ asset ('website/css/style.css')}}>






{{--<link rel="stylesheet" type="text/css" href={{ asset ('website/css/style.css')}}>--}}
{{--<link rel="icon" type="image/png" href="{{('website/img/logo.png')}}" sizes="20*20" />--}}
{{--<link rel="stylesheet" type="text/css" href={{ asset ('website/css/animate.css')}}>--}}
{{--<link rel="stylesheet" type="text/css" href={{ asset ('website/css/all.css')}}>--}}
{{--<link rel="stylesheet" href="{{asset('website/css/style5.css')}}">--}}
