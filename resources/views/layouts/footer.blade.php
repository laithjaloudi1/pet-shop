{{--<footer class="page-footer font-small mdb-color lighten-3 pt-4 foot" style="opacity: 95%;">--}}


{{--    <!-- Footer Links -->--}}
{{--    <div class="container text-center">--}}

{{--        <!-- Grid row -->--}}
{{--        <div class="row">--}}

{{--            <!-- Grid column -->--}}
{{--            <div class="col-md-4 col-lg-3 mr-auto my-md-4 my-0 mt-4 mb-1">--}}

{{--                <!-- Content -->--}}
{{--                <h5 class="font-weight-bold text-uppercase mb-4">Content</h5>--}}
{{--                <p>The PetBite Shop</p>--}}
{{--                <p>We are a small local online company in Jordan-Amman dedicated to providing families with food and supplies for dogs, cats, birds, and fish. We value our clients’ opinions, recommendations and suggestions about the services that we provide.--}}
{{--                </p>--}}
{{--            </div>--}}
{{--            <!-- Grid column -->--}}

{{--            <hr class="clearfix w-100 d-md-none">--}}

{{--            <!-- Grid column -->--}}
{{--            <div class="col-md-2 col-lg-2 mx-auto my-md-4 my-0 mt-4 mb-1">--}}

{{--                <!-- Links -->--}}
{{--                <h5 class="font-weight-bold text-uppercase mb-4">About</h5>--}}

{{--                <ul class="list-unstyled">--}}
{{--                    <li>--}}
{{--                        <p>--}}
{{--                            <a href="{{route('contact-us')}}">contact-us</a>--}}
{{--                        </p>--}}
{{--                    </li>--}}
{{--                    <li>--}}
{{--                        <p>--}}
{{--                            <a href="{{route('about-us')}}">about-us</a>--}}
{{--                        </p>--}}
{{--                    </li>--}}
{{--                </ul>--}}

{{--            </div>--}}
{{--            <!-- Grid column -->--}}

{{--            <hr class="clearfix w-100 d-md-none">--}}

{{--            <!-- Grid column -->--}}
{{--            <div class="col-md-4 col-lg-3 mx-auto my-md-4 my-0 mt-4 mb-1">--}}

{{--                <!-- Contact details -->--}}
{{--                <h5 class="font-weight-bold text-uppercase mb-4">Address</h5>--}}

{{--                <ul class="list-unstyled">--}}
{{--                    <li>--}}
{{--                        <p>--}}
{{--                            <i class="fas fa-home mr-3"></i> Amman, Jordan , JO</p>--}}
{{--                    </li>--}}
{{--                    <li>--}}
{{--                        <p>--}}
{{--                            <i class="fas fa-envelope mr-3"></i> Petbitejo@gmail.com</p>--}}
{{--                    </li>--}}
{{--                    <li>--}}
{{--                        <p>--}}
{{--                            <i class="fas fa-phone mr-3"></i> + 9627 9892 9935--}}
{{--                        </p>--}}
{{--                    </li>--}}
{{--                    <li>--}}
{{--                        <p>--}}
{{--                            <i class="fas fa-print mr-3"></i> + 9627 9507 3383</p>--}}
{{--                    </li>--}}
{{--                </ul>--}}

{{--            </div>--}}
{{--            <!-- Grid column -->--}}

{{--            <hr class="clearfix w-100 d-md-none">--}}

{{--            <!-- Grid column -->--}}
{{--            <div class="col-md-12 col-lg-12 text-center mx-auto my-4">--}}

{{--                <!-- Social buttons -->--}}
{{--                <h5 class="font-weight-bold text-uppercase mb-4">Follow Us</h5>--}}

{{--                <!-- Facebook -->--}}
{{--                <a type="button" href="Facebook.com/tpetbite" class="btn-floating btn-fb">--}}
{{--                    <i class="fab fa-facebook-f"></i>--}}
{{--                </a>--}}
{{--                <!-- Twitter -->--}}
{{--                <a type="button" class="btn-floating btn-tw">--}}
{{--                    <i class="fab fa-twitter"></i>--}}
{{--                </a>--}}
{{--                <!-- instagram +-->--}}
{{--                <a type="button" href="Instagram.com/thepetbite" class="btn-floating btn-gplus">--}}
{{--                    <i class="fab fa-instagram"></i>--}}
{{--                </a>--}}


{{--            </div>--}}
{{--            <!-- Grid column -->--}}

{{--        </div>--}}
{{--        <!-- Grid row -->--}}

{{--    </div>--}}
{{--    <!-- Footer Links -->--}}

{{--    <!-- mali -->--}}
{{--    <div class="footer-copyright text-center py-3">© 2020 mali:--}}
{{--        <span>Copyright © {{date('Y')}} Developed by <a href="https://www.linkedin.com/in/m-ali-abdulwahed-1533b9171/" target='_blank' title="Colorlib" style="color: #00AFF0">M Ali Abdulwahed</a>. All rights reserved.</span>    </div>--}}
{{--    <!-- mali -->--}}

{{--</footer>--}}


<footer class="footer-area bg-black-2 section-padding-1">
    <div class="container-fluid">
        <div class="footer-top pt-80 pb-25">
            <div class="row align-items-center">
                <div class="col-lg-4 col-md-4">
                    <div class="footer-logo footer-logo-ngtv-mrg">
                        <a href="{{route('index')}}">
                            <img class="see" src="{{asset('website/img/petlogo.png')}}" alt="logo">
                        </a>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8">
                    <div class="footer-ml-30">
                        <div class="row">
                            <div class="col-xl-2 col-md-6 col-md-6 col-sm-6 col-6">
                                <div class="footer-widget mb-55">
                                    <h3 class="footer-title-3 pets-footer-fredoka">The PetBite Shop</h3>
                                    <div class="footer-info-list-2">
                                        <ul>
                                            <li><a href="{{route('about-us')}}">About Us</a></li>
                                            <li><a href="{{route('contact-us')}}">Contact Us</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-2 col-md-6 col-md-6 col-sm-6 col-6">
                                <div class="footer-widget mb-55">
                                    <h3 class="footer-title-3 pets-footer-fredoka">Userful</h3>
                                    <div class="footer-info-list-2">
                                        <ul>
                                            <li><a href="Facebook.com/tpetbite">Facebook</a></li>
                                            <li><a href="Instagram.com/thepetbite">Instagram</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-md-6 col-md-6 col-sm-6 col-12">
                                <div class="footer-widget footer-mrg-3 mb-55">
                                    <h3 class="footer-title-3 pets-footer-fredoka">About Shop</h3>
                                    <div class="footera-about">

                                        <p style="text-align: left">We are a small local online company in Jordan-Amman dedicated to providing families with food and supplies for dogs, cats, birds, and fish. We value our clients’ opinions, recommendations and suggestions about the services that we provide.
                                        </p>                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-md-6 col-md-6 col-sm-6 col-12">
                                <div class="footer-widget mb-55">
                                    <h3 class="footer-title-3 pets-footer-fredoka">Contact Info</h3>
                                    <div class="footer-contact-wrap">
                                        <div class="single-footer-contact">
                                            <div class="footer-contact-icon">
                                                <i class="fa fa-clock-o"></i>
                                            </div>
                                            <div class="footer-contact-text">
                                                <p>7/7: 9:00 - 19:00</p>
                                            </div>
                                        </div>
                                        <div class="single-footer-contact">
                                            <div class="footer-contact-icon">
                                                <i class="fa fa-envelope-o"></i>
                                            </div>
                                            <div class="footer-contact-text">
                                                <p> Petbitejo@gmail.com</p>
                                            </div>
                                        </div>
                                        <div class="single-footer-contact">
                                            <div class="footer-contact-icon">
                                                <i class="fa fa-volume-control-phone"></i>
                                            </div>
                                            <div class="footer-contact-text">
                                                <p>+ 9627 9892 9935 // + 9627 9507 3383</p>
                                            </div>
                                        </div>
                                        <div class="single-footer-contact">
                                            <div class="footer-contact-icon">
                                                <i class="fa fa-home"></i>
                                            </div>
                                            <div class="footer-contact-text">
                                                <p>Amman, Jordan , JO</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom footer-bottom-ptb border-top-4 section-padding-1">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-6 col-12">
                    <div class="copyright copyright-gray">
                        <span>Copyright © {{date('Y')}} Developed by <a href="https://www.linkedin.com/in/m-ali-abdulwahed-1533b9171/" target='_blank' title="Colorlib">M Ali Abdulwahed</a>. All rights reserved.</span>
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="social-icon-style social-icon-white social-icon-right-2 res-xs-mt-10">
                        <a href="https://www.instagram.com/thepetbite/"><i class="fa fa-instagram"></i></a>
                        <a href="https://www.facebook.com/Tpetbite"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
