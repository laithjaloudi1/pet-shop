<!DOCTYPE html>
<html>
<head>
    @include('layouts.head')
</head>

<body class="">

<div id='loader'>
    <div class="spinner"></div>
</div>

<script>
    window.addEventListener('load', () => {
        const loader = document.getElementById('loader');
        setTimeout(() => {
            loader.classList.add('fadeOut');
        }, 300);
    });
</script>

    <div class="">
        @include('layouts.header',['categories'=>$categories])
        <div class="" id="">
            @include('layouts.navs.nav')
            <div class="pb-20">
                @yield('content')
            </div>
            <div class="pt-15">
                @include('layouts.footer')
            </div>
        </div>
    </div>


</body>
@include('layouts.scripts')

</html>
