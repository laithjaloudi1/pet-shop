<div class="main-wrapper">
    <header class="header-area bg-light-yellow-2">
        <div class="header-large-device">
            <div class="container">
                <div class="header-ptb">
                    <div class="row align-items-center">
                        <div class="col-lg-4">
                            <div class="search-wrap search-width-1">
                                <form class="search-form" action="/search" method="get">
                                    <input type="search" name="search" placeholder="Search products...">
                                    <button class="button-search"><i class="lnr lnr-magnifier"></i></button>
                                </form>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="logo logo-width text-center">
                                <a href="{{route('index')}}">
                                    <img class = 'see' src="{{asset('website/img/petlogo.png')}}" alt="logo">
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="header-right-wrap header-right-flex">
                                <div class="same-style">
                                    <a class="" href="{{URL::route('website_cart_view')}}"><i class="fa fa-cart-arrow-down"></i></a>
                                </div>
                                <div class="same-style">
                                    <a class="clickalbe-button-active" href="#"><i class="fa fa-bars"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section-padding-2 kid-main-menu-wrap">
                <div class="container-fluid">
                    <div class="bg-img kid-menu-bg-img" style="background-image:url({{asset('website/assets/images/bg/pet-bg-1.png')}});">
                        <div class="main-menu main-menu-padding-3 menu-text-black text-center kid-main-menu menu-border-none">
                            <nav>
                                <ul>
                                    <li><a class="active" href="{{route('index')}}">HOME</a>

                                    </li>
                                    <li><a href="#">PAGES</a>
                                        <ul class="sub-menu-width">
                                            <li><a href="{{route('about-us')}}">About Us</a></li>
                                            <li><a href="{{route('contact-us')}}">Contact Page</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="{{route('categoryshow')}}">SHOP</a>
                                        <ul class="mega-menu-style-2 mega-menu-width2 menu-negative-mrg5">
                                            <li class="mega-menu-sub-width20"><a class="menu-title" href="#">Categories</a>
                                                <ul>
                                                    <ul>
                                                        @foreach($categories as $category)-
                                                        <li><a href="{{route('website_category',$category->id)}}">{{$category->title_en}}></a></li>
                                                        @endforeach
                                                    </ul>
                                                </ul>
                                            </li>

                                            <li class="mega-menu-sub-width37">
                                                <div class="banner-menu-content-wrap default-overlay">
                                                    <a href=""><img src="{{asset('website/img/undraw_woman_mevk.svg')}}" alt="banner"></a>
                                                    <div class="banner-menu-content">
                                                        <h2>New <br>Collections</h2>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>

                                    <li><a href="">My Account</a>
                                        <ul class="sub-menu-width">
{{--                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">--}}
                                                {{--                        <a class="dropdown-item" href="{{ route('login') }}">login</a>--}}

                                                {{--                        <a class="dropdown-item" href="{{ route('logout') }}"    onclick="event.preventDefault(); document.getElementById('logout-form').submit();"--}}
                                                {{--                           class="d-b td-n pY-5 bgcH-grey-100 c-grey-700">--}}
                                                {{--                            <i class="ti-power-off mR-10"></i>--}}
                                                {{--                            <span>    {{ __('Logout') }} </span>--}}
                                                {{--                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
                                                {{--                                @csrf--}}
                                                {{--                            </form>--}}
                                                {{--                        </a>--}}
                                                {{--                        <a class="dropdown-item" href="{{ route('register') }}">register</a>--}}
                                                {{--                    </div>--}}

                                           @if(Auth::user())
                                                <li><a href="{{ route('logout') }}"    onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                                                       class="d-b td-n pY-5 bgcH-grey-100 c-grey-700">
                                                        <i class="ti-power-off mR-10"></i>
                                                        <span>    {{ __('Logout') }} </span>
                                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                            @csrf
                                                        </form>
                                                    </a></li>
                                            @else
                                                <li><a href="{{ route('login') }}">login</a></li>
                                                <li><a href="{{ route('register') }}">register</a></li>
                                               @endif
                                        </ul>
                                    </li>
                                    <li><a href="{{route('contact-us')}}">CONTACT</a></li>
                                    <li><a href="{{route('about-us')}}">About Us</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-small-device">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-6">
                        <div class="mobile-logo mobile-logo-width">
                            <a href="{{route('index')}}">
                                <img class="imge" alt="" src="{{asset('website/img/petlogo.png')}}" style="height: 2.5rem">
                            </a>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="header-right-wrap header-right-flex">
                            <div class="same-style">
                                <a class="" href="{{URL::route('website_cart_view')}}"><i class="fa fa-cart-arrow-down"></i></a>
                            </div>
                            <div class="same-style">
                                <a class="mobile-menu-button-active" href="#"><i class="fa fa-bars"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>











{{--<nav class="navbar navbar-expand-lg navbar-light bg-light">--}}
{{--            <button type="button" id="sidebarCollapse" class="navbar-btn">--}}
{{--                <span></span>--}}
{{--                <span></span>--}}
{{--                <span></span>--}}
{{--            </button>--}}

{{--    <a class="navbar-brand d-lg-none" href="#"><img class="seee" id="" src="{{{asset('website/img/logo.png')}}}"></a>--}}


{{--    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggle" aria-controls="navbarToggle" aria-expanded="false" aria-label="Toggle navigation">--}}
{{--        <span class="navbar-toggler-icon"></span>--} }
{{--    </button>--}}


{{--    <div class="collapse navbar-collapse justify-content-between" id="navbarToggle">--}}

{{--        <ul class="navbar-nav">--}}
{{--            <li class="nav-item">--}}
{{--               <a class="nav-link active" href="{{route('index')}} "><i class="fas fa-home"></i>Home <span class="sr-only">(current)</span></a>--}}
{{--            </li>--}}
{{--            <li class="nav-item">--}}
{{--                <a class="nav-link active" href="{{route('website_cart_view')}}"><i class="fas fa-shopping-cart"></i>Cart <span class="sr-only">(current)</span></a>--}}
{{--            </li>--}}
{{--        </ul>--}}



{{--        <img class="see navbar-brand d-none d-lg-block" id="logo" src="{{{asset('website/img/logo.png')}}}">--}}


{{--        <ul class="navbar-nav">--}}
{{--            <li class="nav-item">--}}
{{--                <div class="dropdown">--}}
{{--                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
{{--                        users--}}
{{--                    </button>--}}
{{--                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">--}}
{{--                        <a class="dropdown-item" href="{{ route('login') }}">login</a>--}}

{{--                        <a class="dropdown-item" href="{{ route('logout') }}"    onclick="event.preventDefault(); document.getElementById('logout-form').submit();"--}}
{{--                           class="d-b td-n pY-5 bgcH-grey-100 c-grey-700">--}}
{{--                            <i class="ti-power-off mR-10"></i>--}}
{{--                            <span>    {{ __('Logout') }} </span>--}}
{{--                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
{{--                                @csrf--}}
{{--                            </form>--}}
{{--                        </a>--}}
{{--                        <a class="dropdown-item" href="{{ route('register') }}">register</a>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </li>--}}
{{--            <li>--}}

{{--                <ul class="dropdown-menu fsz-sm">--}}
{{--                    <li>--}}
{{--                        <a href="" class="d-b td-n pY-5 bgcH-grey-100 c-grey-700">--}}
{{--                            <i class="ti-user mR-10"></i>--}}
{{--                            <span>Profile</span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li role="separator" class="divider"></li>--}}
{{--                    <li>--}}
{{--                        <a href="{{ route('login') }}"    onclick="event.preventDefault(); document.getElementById('logout-form').submit();"--}}
{{--                           class="d-b td-n pY-5 bgcH-grey-100 c-grey-700">--}}
{{--                            <i class="ti-power-off mR-10"></i>--}}
{{--                            <span>    {{ __('login') }} </span>--}}
{{--                            <form id="logout-form" action="{{ route('login') }}" method="POST" style="display: none;">--}}
{{--                                @csrf--}}
{{--                            </form>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li>--}}
{{--                        <a href="{{ route('logout') }}"    onclick="event.preventDefault(); document.getElementById('logout-form').submit();"--}}
{{--                           class="d-b td-n pY-5 bgcH-grey-100 c-grey-700">--}}
{{--                            <i class="ti-power-off mR-10"></i>--}}
{{--                            <span>    {{ __('Logout') }} </span>--}}
{{--                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
{{--                                @csrf--}}
{{--                            </form>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                </ul>--}}


{{--            </li>--}}
{{--        </ul>--}}
{{--    </div>--}}
{{--</nav>--}}
        <div class="mobile-menu-active clickalbe-sidebar-wrapper-style-1">
            <div class="clickalbe-sidebar-wrap">
                <a class="sidebar-close"><i class=" ti-close "></i></a>
                <div class="mobile-menu-content-area sidebar-content-100-percent">
                    <div class="mobile-search">
                        <form class="search-form" action="#">
                            <input type="search" name="search" placeholder="Search products...">
                            <button class="button-search"><i class=" ti-search "></i></button>
                        </form>
                    </div>
                    <div class="clickable-mainmenu-wrap clickable-mainmenu-style1">
                        <nav>
                            <ul>
                                <li><a class="active" href="{{route('index')}}">HOME</a></li>
                                <li><a href="{{route('website_cart_view')}}">Cart</a></li>
                                <li><a href="{{route('categoryshow')}}">Shop</a></li>
                                <li class="has-sub-menu"><a href="#">Categories</a>
                                    <ul class="sub-menu-2">
                                        @foreach($categories as $category)-
                                        <li><a href="{{route('website_category',$category->id)}}">{{$category->title_en}}></a></li>
                                        @endforeach
                                    </ul>
                                </li>
                                <li class="has-sub-menu"><a href="#">Pages</a>
                                    <ul class="sub-menu-2">
                                        <li><a href="{{route('about-us')}}">About Us</a></li>
                                        <li><a href="{{route('contact-us')}}">Contact Page</a></li>

                                    </ul>
                                </li>
                                        <li class="has-sub-menu"><a href="#">My Account</a>
                                            <ul class="sub-menu-2">
                                                <li><a href="">My Account</a></li>
                                                @if(Auth::user())
                                                    <li><a href="{{ URL::route('logout') }}"    onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                                                           class="d-b td-n pY-5 bgcH-grey-100 c-grey-700">
                                                            <i class="ti-power-off mR-10"></i>
                                                            <span>    {{ __('Logout') }} </span>
                                                            <form id="logout-form" action="{{ URL::route('logout') }}" method="POST" style="display: none;">
                                                                @csrf
                                                            </form>
                                                        </a></li>
                                                @else
                                                    <li><a href="{{ URL::route('login') }}">login</a></li>
                                                    <li><a href="{{URL:: route('register') }}">register</a></li>
                                                @endif
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <div class="header-aside-content">
                        <div class="header-aside-payment">
                            <img src="{{asset('website/assets/images/icon-img/payment.png')}}" alt="payment">
                        </div>
                        <p style="text-align: left">We are a small local online company in Jordan-Amman dedicated to providing families with food and supplies for dogs, cats, birds, and fish. We value our clients’ opinions, recommendations and suggestions about the services that we provide.
                        <div class="aside-contact-info">
                            <ul>
                                <li><i class=" ti-alarm-clock "></i>7/7: 9:00 - 19:00</li>
                                <li><i class=" ti-email "></i>Petbitejo@gmail.com</li>
                                <li><i class=" ti-mobile "></i>+ 9627 9892 9935 // + 9627 9507 3383</li>
                                <li><i class=" ti-home "></i>Amman, Jordan , JO</li>
                            </ul>
                        </div>
                        <div class="social-icon-style-2 mb-25">
                            <a href="Instagram.com/thepetbite"><i class="fa fa-instagram"></i></a>
                            <a href="Facebook.com/tpetbite"><i class="fa fa-facebook"></i></a>
                            <a class="twitter" href="#"><i class="fa fa-twitter"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
</div>

