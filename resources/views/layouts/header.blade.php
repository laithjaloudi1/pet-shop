{{--<nav id="sidebar" style="opacity: 95%;">--}}
{{--    <div class="sidebar-header">--}}
{{--        <h3>The PetBite</h3>--}}
{{--    </div>--}}

{{--    <ul class="list-unstyled components" style="margin-right: 60px">--}}
{{--        <p><i class="fas fa-truck"></i> Free Delivery</p>--}}
{{--        <li class="active">--}}
{{--            <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Home</a>--}}
{{--            <ul class="collapse list-unstyled" id="homeSubmenu">--}}
{{--                <li>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="#">Home 2</a>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="#">Home 3</a>--}}
{{--                </li>--}}
{{--            </ul>--}}
{{--        </li>--}}
{{--        <li>--}}
{{--            <a href="{{{route('index')}}}"><i class="fas fas fa-home"></i>Home</a>--}}
{{--            <a class="nav-link active" href="{{route('website_cart_view')}}"><i class="fas fa-shopping-cart"></i>Cart <span class="sr-only">(current)</span></a>--}}
{{--            <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Categories</a>--}}
{{--            <ul class="collapse list-unstyled" id="pageSubmenu">--}}
{{--                @foreach($categories as $category)--}}
{{--                <li>--}}
{{--                    <a href="{{route('website_category',$category->id)}}">{{$category->title_en}}</a>--}}
{{--                </li>--}}
{{--                @endforeach--}}
{{--            </ul>--}}
{{--        </li>--}}
{{--        <li>--}}
{{--            <a href="{{route('about-us')}}">about-us</a>--}}
{{--        </li>--}}
{{--        <li>--}}
{{--            <a href="{{route('contact-us')}}">Contact-us</a>--}}
{{--        </li>--}}
{{--    </ul>--}}

{{--    <ul class="list-unstyled CTAs">--}}
{{--        <li>--}}
{{--            <a href="Facebook.com/tpetbite"><i class="fab fa-facebook-f"></i> Facebook</a>--}}
{{--        </li>--}}
{{--        <li>--}}
{{--            <a href="Instagram.com/thepetbite"><i class="fab fa-instagram"></i> Instagram</a>--}}
{{--        </li>--}}
{{--    </ul>--}}
{{--</nav>--}}


<div class="clickalbe-sidebar-wrapper-active clickalbe-sidebar-wrapper-style-1">
    <div class="clickalbe-sidebar-wrap clickalbe-sidebar-padding-dec">
        <a class="sidebar-close"><i class=" ti-close "></i></a>
        <div class="header-aside-content sidebar-content-100-percent">
            <div class="header-aside-menu">
                <nav>
                    <ul>
                        <li><a href="{{route('about-us')}}">About PetBite</a></li>
                        <li><a href="{{route('contact-us')}}">Contact us</a></li>
                    </ul>
                </nav>
            </div>
            <div class="header-aside-payment">
                <img src="{{asset('website/assets/images/icon-img/payment.png')}}" alt="payment">
            </div>
            <p>The PetBite Shop</p>
                            <p>We are a small local online company in Jordan-Amman dedicated to providing families with food and supplies for dogs, cats, birds, and fish. We value our clients’ opinions, recommendations and suggestions about the services that we provide.
                            </p>
            <div class="aside-contact-info">
                <ul>
                    <li><i class=" ti-alarm-clock "></i>7/7: 9:00 - 19:00</li>
                    <li><i class=" ti-email "></i> Petbitejo@gmail.com</li>
                    <li><i class=" ti-mobile "></i>+ 9627 9892 9935</li>
                    <li><i class=" ti-mobile "></i>+ 9627 9507 3383</li>
                    <li><i class=" ti-home "></i>Amman, Jordan , JO</li>
                </ul>
            </div>
            <div class="social-icon-style-2 mb-25">
                <a class="facebook" href="Facebook.com/tpetbite"><i class="fa fa-facebook"></i></a>
                <a class="twitter" href="Instagram.com/thepetbite"><i class="fa fa-twitter"></i></a>
            </div>
            <div class="copyright copyright-gray-2">
                <span>Copyright © {{date('Y')}} Developed by <a href="https://www.linkedin.com/in/m-ali-abdulwahed-1533b9171/" target='_blank' title="Colorlib">M Ali Abdulwahed</a>. All rights reserved.</span>
            </div>
        </div>
    </div>
</div>


