<!-- jquery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
<!-- Popper JS -->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<!-- Bootstrap JS -->
<script src="{{asset('website/assets/js/plugins/owl-carousel.js')}}"></script>
<script src="{{asset('website/assets/js/plugins/slick.js')}}"></script>
<script src="{{asset('website/assets/js/plugins/jquery.mb.ytplayer.min.js')}}"></script>
<script src="{{asset('website/assets/js/plugins/magnific-popup.js')}}"></script>
<script src="{{asset('website/assets/js/plugins/wow.js')}}"></script>
<script src="{{asset('website/assets/js/plugins/instafeed.js')}}"></script>
<script src="{{asset('website/assets/js/plugins/countdown.js')}}"></script>
<script src="{{asset('website/assets/js/plugins/jarallax.min.js')}}"></script>
<script src="{{asset('website/assets/js/plugins/images-loaded.js')}}"></script>
<script src="{{asset('website/assets/js/plugins/isotope.js')}}"></script>
<script src="{{asset('website/assets/js/plugins/tilt.js')}}"></script>
<script src="{{asset('website/assets/js/plugins/jquery-ui-touch-punch.js')}}"></script>
<script src="{{asset('website/assets/js/plugins/jquery-ui.js')}}"></script>
<script src="{{asset('website/assets/js/plugins/easyzoom.js')}}"></script>
<script src="{{asset('website/assets/js/plugins/resizesensor.js')}}"></script>
<script src="{{asset('website/assets/js/plugins/sticky-sidebar.js')}}"></script>
<script src="{{asset('website/assets/js/plugins/scrollup.js')}}"></script>
<script src="{{asset('website/assets/js/plugins/ajax-mail.js')}}"></script>
<!-- Main JS -->
<script src="{{asset('website/assets/js/main.js')}}"></script>

{{--<script src="{{ asset ('website/jas/bootstrap.js')}}"></script>--}}

<script src="{{ asset ('website/jas/slid.js')}}"></script>


<!-- All JS is here
============================================ -->

<!-- Modernizer JS -->
<script src="{{asset('website/assets/js/vendor/modernizr-3.6.0.min.js')}}"></script>



<script>
    // optional
    $('#blogCarousel').carousel({
        interval: 5000
    });
</script>
