<div class="row">
    <div class="col-sm-12">
{{--        @dd($errors)--}}
        @if (count($errors) > 0)
            <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-danger alert-dismissible fade show"
                 role="alert">
                <div class="m-alert__icon">
                    <i class="flaticon-exclamation-1"></i>
                    <span></span>
                </div>
                <div class="m-alert__text">
                    <strong>Please fix theses errors:</strong>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                <div class="m-alert__close">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    </button>
                </div>
            </div>
        @endif
        @if((Session::has('error') || isset($error)) && count($errors)==0)
            <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-danger alert-dismissible fade show"
                 role="alert">
                <div class="m-alert__icon">
                    <i class="flaticon-exclamation-1"></i>
                    <span></span>
                </div>
                <div class="m-alert__text">
                    <strong>Please fix theses errors:</strong>

                    {!! Session::get('error','') !!}<br>
                    {!! isset($error)?$error:'' !!}

                </div>
                <div class="m-alert__close">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    </button>
                </div>
            </div>
        @endif
        @if(Session::has('message') || isset($message) || isset($success)||Session::has('sessionMessage'))
            <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-success alert-dismissible fade show"
                 role="alert">
                <div class="m-alert__icon">
                    <i class="la la-check"></i>
                    <span></span>
                </div>
                <div class="m-alert__text">
                    <strong>Well Done:</strong>
                    {!! Session::get('message','') !!}<br>
                    {!! isset($message)?$message:'' !!}
                    {!! isset(Session::get('sessionMessage')['message'])?Session::get('sessionMessage')['message']:'' !!}

                </div>
                <div class="m-alert__close">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    </button>
                </div>
            </div>
        @endif

        @if(Session::has('warning') || isset($warning) )
            <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-warning alert-dismissible fade show"
                 role="alert">
                <div class="m-alert__icon">
                    <i class="flaticon-danger"></i>
                    <span></span>
                </div>
                <div class="m-alert__text">
                    <strong>Warning :</strong>
                    {!! Session::get('warning','') !!}<br>
                    {!! isset($warning)?$warning:'' !!}
                </div>
                <div class="m-alert__close">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    </button>
                </div>
            </div>
        @endif
        @if(Session::has('info') || isset($info) )
            <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-info alert-dismissible fade show"
                 role="alert">
                <div class="m-alert__icon">
                    <i class="flaticon-exclamation-1"></i>
                    <span></span>
                </div>
                <div class="m-alert__text">
                    <strong>Info :</strong>
                    {!! Session::get('info','') !!}<br>
                    {!! isset($info)?$info:'' !!}
                </div>
                <div class="m-alert__close">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    </button>
                </div>
            </div>
        @endif
    </div>
</div>
