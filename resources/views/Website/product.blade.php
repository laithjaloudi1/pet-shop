@extends('layouts.app')
@section('content')


            @foreach($product as $oneproduct)
                <div class="container" id="product-section">
                    <div class="row pt-150">
                <div class="col-md-6">
                    <img class = 'imge' src="{{asset("storage/{$oneproduct->image}")}}" alt="product image" class="image-responsive" style="border-style:solid;border-width: 0.5px; margin-right: 19%;padding-top: 10%;width: 100%"/>
                </div>
                <div class="col-md-6 ">
                    <div class="row">
                        <div class="col-md-12">
                            <h1>{{$oneproduct->title_en}}</h1>
                            <hr class="solid">
                        </div>
                    </div><!-- end row-->
                    <div class="row">
                        <div class="col-md-12">
                            <p class="description">
                            <h4>product description</h4>
                            {{$oneproduct->description_en}}</p>
                            <hr class="solid">
                        </div>
                    </div><!-- end row -->
                    <form method="post" action="{{route('website_cart_store',$oneproduct->id)}}">
                    <div class="row">
                        <div class="col-md-8 bottom-rule">
                            <h2 class="product-price">{{$oneproduct->price }}JD</h2>
                        </div>
                        <div class="col-md-4 col-md-offset-1 text-center">

                                @csrf
                                <button type="submit" id="btnShowMsg" value="Click Me!" onClick='showMessage()' class="btn btn-primary btn-block btn-sm">Add to Cart</button>

                        </div>


                    </div><!-- end row -->
                    <hr class="solid">
                    <div class="row" style="margin-top: 15px;">
                        @if($oneproduct->attributes->count()>0)
                            @foreach($oneproduct->attributes as $items)
                            <fieldset class="form-group">
                                <div class="row">
                                    <label class="col-form-legend col-sm-2 pr-5" style="margin-right: 5px">{{$items->name_en}}</label>
                                    <div class="col-sm-10">
                                        @foreach($items->values as $item)
                                            <div class = 'item' style=" .item {white-space: nowrap;display:inline  }">
                                                <div class="form-check">
                                                    <label class="form-check-label pt-4 pl-4 pr-4">
                                                        <input class="form-check-input pl-10" type="radio" name="values[{{$items->id}}]" id="gridRadios1" value="{{$item->id}}" checked>
                                                        {{$item->value}}
                                                    </label>
                                                </div>
                                            </div>
                                            @endforeach
                                    </div>
                                </div>
                            </fieldset>
                            @endforeach
                        @endif
                    </form>
                    </div>
                </div>
                    </div>
                @endforeach
@endsection
<script type="text/javascript">
    function showMessage(){
        alert("Product added successfully.");
    }
</script>

<style>
    input[type='radio'] {
        float: left;
        width: 20px;
    }
    input[type='radio'] + label {
        display: block;
        width: 30px;
    }
</style>
