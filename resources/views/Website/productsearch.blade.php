@extends('layouts.app')
@section('content')
    <div class="row padding pt-150">
        @foreach($product as $products)
            <div class="col-md-4">
                <div class="product-card">
                    <div class="product-image">
                        <img class="card-img-top imge" src="{{asset("storage/{$products->image}")}}" alt="product image">
                    </div>
                    <div class="product-info">
                        <h4 class="card-title">{{$products->title_en}}</h4>
                        <h6 class="card-text">{{$products->price}}JD</h6>
                        <a href="{{route('website_product',$products->id)}}"
                           class="btn btn-outline-secondary">Show</a>

                    </div>
                </div>
            </div>
        @endforeach
    </div>
    @endsection
