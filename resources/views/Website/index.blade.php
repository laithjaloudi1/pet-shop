@extends('layouts.app')
@section('content')
{{--    <div class="container-fluid">--}}
{{--        <section class="" id="">--}}
{{--            <img class="se pb-3 animated lightSpeedIn slower" id="logo" src="{{{asset('website/img/logo.png')}}}">--}}
{{--            <div class="bd-example">--}}
{{--                <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">--}}
{{--                    <ol class="carousel-indicators">--}}
{{--                        <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>--}}
{{--                        <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>--}}
{{--                        <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>--}}
{{--                    </ol>--}}
{{--                    <div class="carousel-inner">--}}
{{--                        <div class="carousel-item active">--}}
{{--                            <img src="{{{asset('website/img/animal1.png')}}}" class="d-block w-100" alt="1st image">--}}
{{--                            <div class="carousel-caption d-none d-md-block">--}}
{{--                                <h1 class="display-2" style="color: white">All animals needs at one place</h1>--}}
{{--                                <h3 style="color: white">Shop Now</h3>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="carousel-item">--}}
{{--                            <img src="{{{asset('website/img/animal.png')}}}" class="d-block w-100" alt="2nd image">--}}
{{--                            <div class="carousel-caption d-none d-md-block">--}}
{{--                                <h1 class="display-2" style="color: white">All animals needs at one place</h1>--}}
{{--                                <h3 style="color: white">Shop Now</h3>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="carousel-item">--}}
{{--                            <img src="{{{asset('website/img/animal2.png')}}}" class="d-block w-100" alt="3rd image">--}}
{{--                            <div class="carousel-caption d-none d-md-block">--}}
{{--                                <h1 class="display-2" style="color: white">All animals needs at one place</h1>--}}
{{--                                <h3>Shop Now</h3>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">--}}
{{--                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>--}}
{{--                        <span class="sr-only">Previous</span>--}}
{{--                    </a>--}}
{{--                    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">--}}
{{--                        <span class="carousel-control-next-icon" aria-hidden="true"></span>--}}
{{--                        <span class="sr-only">Next</span>--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <h1 class="animated bounceIn slower pt-3">Online Pet Shop At Jordan</h1>--}}

{{--            <p class="subhead animated bounceIn slower">We are a small local online company in Jordan-Amman dedicated to providing families with food and supplies for dogs, cats, birds, and fish. We value our clients’ opinions, recommendations and suggestions about the services that we provide.</p>--}}
{{--            <h1 class="animated bounceIn slower">Cash On deleviry</h1>--}}
{{--        </section>--}}
{{--    </div>--}}


<div class="slider-area pet-slider-area section-padding-6 bg-light-yellow-2">
    <div class="container-fluid">
        <div class="bg-img pet-single-slider-wrap slider-height-7" style="background-image:url({{asset('website/assets/images/bg/pet-bg-2.png')}});">
            <div class="single-main-slider slider-animated-1 align-items-center custom-d-flex single-main-slider-gradient">
                <div class="row no-gutters width-100-percent">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                        <div class="main-hero-content-5 text-center">
                            <h1 class="animated wow fadeInDown" data-wow-delay=".5s">A pet store with <br>everything you need. </h1>
                            <div class="btn-style-3 btn-hover-2 wow fadeInUp" data-wow-delay=".5s">
                                <a class="animated bs3-white-text bs3-yellow-bg bs3-ptb bs3-ptb bs3-border-radius ptb-2-white-hover" href="{{route('categoryshow')}}">Shop now</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slider-pet-img wow fadeInRight" data-wow-delay=".5s">
                    <img src="{{asset('website/img/96115498_1196357280706564_65710873619988480_n.png')}}" alt="slider">
                </div>
            </div>
        </div>
        <div class="pets-contact-wrap">
            <div class="pets-social">
                <a class="pets-facebook" href="https://www.facebook.com/Tpetbite"><img src="{{asset('website/assets/images/icon-img/pet-1.png')}}" alt=""> facebook</a>
                <a class="pets-instagram" href="https://www.instagram.com/thepetbite/"><img src="{{asset('website/assets/images/icon-img/pet-1.png')}}" alt=""> instagram</a>
                <a class="pets-twitter" href=""><img src="{{asset('website/assets/images/icon-img/pet-1.png')}}" alt=""> twitter</a>
            </div>
            <div class="pets-phn-number">
                <p><img src="{{asset('website/assets/images/icon-img/pet-2.png')}}" alt=""> Call:  +9627 9892 9935 </p>
            </div>
        </div>
    </div>
    <div class="pets-slider-shape">
        <img src="{{asset('website/img/pets-slider-shape.png')}}" alt="">
    </div>
</div>
<div class="product-area pets-product-area pt-100 pb-55">
    <div class="container">
        <div class="section-title-9 text-center mb-45">
            <h2>New pets</h2>
        </div>
    </div>
    <div class="section-padding-1">
        <div class="container-fluid">
            <div class="product-tab-list-3 nav mb-50">
                <a class="active" href="#product-1" data-toggle="tab">
                    New Arrivales
                </a>
                <a href="#product-2" data-toggle="tab">
                    Brands
                </a>
            </div>
{{--            content--}}

            <div class="tab-content jump padding-55-row-col">
                <div id="product-1" class="tab-pane active">
                    <div class="row">
                        @foreach($product as $products)
                        <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                            <div class="product-wrap mb-55">
                                <div class="product-img pro-border-none pro-border-2 mb-15">

                                        <img class = 'imge' src="{{asset("storage/{$products->image}")}}" alt="" style=" border-radius: 5%">

                                    <div class="product-action product-action-position-1 pro-action-yellow-2">

                                    </div>
                                </div>
                                <div class="product-content-3">
                                    <h4 class="card-title">{{$products->title_en}}</h4>
                                    <a href="{{route('website_product',$products->id)}}"
                                       class="btn btn-outline-secondary">Show</a>
                                    <div class="product-price-3">
                                        <span class="new-price"> <h6 class="card-text">{{$products->price}}JD</h6></span>
                                    </div>

                                </div>
                            </div>
                        </div>
                            @endforeach
                    </div>
                </div>
                <div id="product-2" class="tab-pane">
                    <div class="row">
                        @foreach($brands as $brand)
                            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12">
                                <div class="product-wrap mb-55">
                                    <div class="product-img pro-border-none pro-border-2 mb-15">
                                        <img class = 'imge' src="{{asset("storage/{$brand->image}")}}">
                                    </div>
                                    <div class="product-content-3">
                                        <h4>{{$brand->title_en}}</h4>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{--            endcontent--}}

{{--                        contentbrand--}}




{{--                        endcontentbrand--}}


<div class="pets-choose-area pets-choose-ptb bg-img" style="background-image:url({{asset('website/img/undraw_good_doggy_4wfq.svg')}});">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12">
                <div class="pets-choose-content">
                    <h2>Why choose us ?</h2>
                    <h2 style="color: black;text-align: left">Our Objectives</h2>
                    <p style="color: black;text-align: left">Our main obejctive from this site is to offer all animals needs at one place,and give our customers an easy way to shop.</p>
                    <p style="color: black;text-align: left">In addition,deliver all your needs to your doors with cash on delivery payment method</p>
                    <h2 style="color: black;text-align: left">How to use our website:</h2>
                    <p style="color: black;text-align: left"><>choose animal name from sidebar then choose your category and type ,add to cart what ever product you want then enter your information and confirm the operation<br>
                        We put into this website all our contact information(number,email,social media)</p>
                    </p>
                    <p style="color: black;text-align: left">delivery is free only inside jordan.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="service-area pt-105 pb-70">
    <div class="container">
        <div class="section-title-9 text-center mb-45">
            <h2>Pet care services</h2>
        </div>
        <div class="service-wrap-3">
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <div class="single-service-3 text-center mb-30">
                        <div class="service-icon-3">
                            <img class = '' src="{{asset('website/img/96239890_253347522530441_8383318271033606144_n.png')}}" alt="icon">
                        </div>
                        <div class="service-content-3">
                            <h3>Pet Service #1</h3>
                            <p>Food</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="single-service-3 text-center mb-30">
                        <div class="service-icon-3">
                            <img class = '' src="{{asset('website/img/96159617_2930832493660969_2789533481535799296_n.png')}}" alt="icon">
                        </div>
                        <div class="service-content-3">
                            <h3>Pet Service #2</h3>
                            <p>Accessories</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="single-service-3 text-center mb-30">
                        <div class="service-icon-3">
                            <img class = '' src="{{asset('website/img/96518867_259207865192303_4593109632037158912_n.png')}}" alt="icon">
                        </div>
                        <div class="service-content-3">
                            <h3>Pet Service #3</h3>
                            <p>Treats</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="testimonial-area-2 testimonial-5-ptb bg-img" style="background-image:url({{asset('website/assets/images/bg/pets-bg-2.png);')}}">
    <div class="container">
        <div class="section-title-9 text-center mb-60">
            <h2>What to say?</h2>
        </div>
        <div class="testimonial-active owl-carousel">
            <div class="single-testimonial-5 text-center">
                <div class="testimonial-quote-5">
                    <img class='imge'src="{{asset('website/img/undraw_loading_frh4.svg')}}" alt="">
                </div>
                <p>Welcome to The PetBite online shop </p>
                <div class="client-info-5">

                </div>
            </div>
            <div class="single-testimonial-5 text-center">
                <div class="testimonial-quote-5">

                </div>
                <p>Trust Is Every Thing .</p>
                <div class="client-info-5">
                    <img class='imge' src="{{asset('website/img/undraw_passing_by_gqfk.svg')}}" alt="">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="instagram-area section-padding-5">
    <div class="container-fluid p-0">
        <div class="row no-gutters">
            <div class="col-12">
                <div class="section-title-9 st-dec-2-font text-center mb-60">
                    <h2>Instagram Shop</h2>
                </div>
                <div class="instagram-feed-thumb-3">
                    <div id="Instafeed-3" class="instagram-wrap-3">
                        <div class="single-instafeed-3"><li><a href="https://www.instagram.com/p/B_f1TyAFUOT/?igshid=1aift7tsljetz" target="_new"><img src="{{asset('website/img/instagram1.png')}}" /><i class="fa fa-search"></i></a></li></div>
                        <div class="single-instafeed-3"><li><a href="https://www.instagram.com/p/B_dVWOSlx3f/?igshid=16a0hzc4b7q18" target="_new"><img src="{{asset('website/img/rn.png')}}" /><i class="fa fa-search"></i></a></li></div>
                        <div class="single-instafeed-3"><li><a href="https://www.instagram.com/p/B_dUctKFzin/?igshid=1xizk3daqq81w" target="_new"><img src="{{asset('website/img/ss.png')}}" /><i class="fa fa-search"></i></a></li></div>
                        <div class="single-instafeed-3"><li><a href="https://www.instagram.com/p/B_dTCpwlmuz/?igshid=1q331c0t16roe" target="_new"><img src="{{asset('website/img/dog.png')}}" /><i class="fa fa-search"></i></a></li></div>
                        <div class="single-instafeed-3"><li><a href="https://www.instagram.com/p/B_dS6aOFbhh/?igshid=708j8zli6hpy" target="_new"><img src="{{asset('website/img/dog2.png')}}" /><i class="fa fa-search"></i></a></li></div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


{{--    <h1 class="pt3">Our Brands</h1>--}}

{{--    <div id="demo" class="carousel slide" data-ride="carousel">--}}

{{--        <!-- Indicators -->--}}
{{--        <ul class="carousel-indicators">--}}
{{--            <li data-target="#demo" data-slide-to="0" class="active"></li>--}}
{{--            <li data-target="#demo" data-slide-to="1"></li>--}}
{{--            <li data-target="#demo" data-slide-to="2"></li>--}}
{{--        </ul>--}}

{{--        <!-- The slideshow -->--}}
{{--        <div class="container carousel-inner no-padding">--}}
{{--            <div class="carousel-item active">--}}

{{--            </div>--}}
{{--    </div>--}}
{{--        <!-- Left and right controls -->--}}
{{--        <a class="carousel-control-prev" href="#demo" data-slide="prev">--}}
{{--            <span class="carousel-control-prev-icon"></span>--}}
{{--        </a>--}}
{{--        <a class="carousel-control-next" href="#demo" data-slide="next">--}}
{{--            <span class="carousel-control-next-icon"></span>--}}
{{--        </a>--}}
{{--    </div>--}}


{{--    <h1 class="pt3">Our New Arrivals Products</h1>--}}
{{--        <div id="demo" class="carousel slide" data-ride="carousel">--}}
{{--        <!-- Indicators -->--}}
{{--        <ul class="carousel-indicators">--}}
{{--            <li data-target="#demo" data-slide-to="0" class="active"></li>--}}
{{--            <li data-target="#demo" data-slide-to="1"></li>--}}
{{--            <li data-target="#demo" data-slide-to="2"></li>--}}
{{--        </ul>--}}

{{--        <!-- The slideshow -->--}}
{{--        <div class="container carousel-inner no-padding">--}}
{{--            <div class="carousel-item active">--}}
{{--                @foreach($product as $products)--}}
{{--                    <div class="col-xs-3 col-sm-3 col-md-3">--}}
{{--                        <img class = 'imge' src="{{asset("storage/{$products->image}")}}">--}}
{{--                        <div class="product-info">--}}
{{--                            <h4 class="card-title">{{$products->title_en}}</h4>--}}
{{--                            <h6 class="card-text">{{$products->price}}JD</h6>--}}
{{--                            <a href="{{route('website_product',$products->id)}}"--}}
{{--                               class="btn btn-outline-secondary">Show</a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                @endforeach--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <!-- Left and right controls -->--}}
{{--        <a class="carousel-control-prev" href="#demo" data-slide="prev">--}}
{{--            <span class="carousel-control-prev-icon"></span>--}}
{{--        </a>--}}
{{--        <a class="carousel-control-next" href="#demo" data-slide="next">--}}
{{--            <span class="carousel-control-next-icon"></span>--}}
{{--        </a>--}}
{{--    </div>--}}


{{--    <!--- Two Column Section -->--}}
{{--    <div class="container-fluid padding">--}}
{{--        <div class="row padding">--}}
{{--            <div class="col-lg-6 pt-4" style="">--}}
{{--                <h2 style="color: black;text-align: left">Our Objectives</h2>--}}
{{--                <p style="color: black;text-align: left">Our main obejctive from this site is to offer all animals needs at one place,and give our customers an easy way to shop.</p>--}}
{{--                <p style="color: black;text-align: left">In addition,deliver all your needs to your doors with cash on delivery payment method</p>--}}
{{--                <h2 style="color: black;text-align: left">How to use our website:</h2>--}}
{{--                <p style="color: black;text-align: left"><>choose animal name from sidebar then choose your category and type ,add to cart what ever product you want then enter your informayion and confirm the operation<br>--}}
{{--                    We put into this website all our contact information(number,email,social media)</p>--}}
{{--                  </p>--}}
{{--                <p style="color: black;text-align: left">deleviry is free only inside jordan.</p>--}}
{{--            </div>--}}
{{--            <div class="col-lg-6">--}}
{{--                <img src="{{{asset('website/img/image1.svg')}}}" class="img-fluid" alt="gi">--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <hr class="my-4">--}}
{{--    </div>--}}


{{--    <button class="fun" data-toggle="collapse" data-target="#emoji">Click to make animals happy</button>--}}
{{--    <div id="emoji" class="collapse">--}}
{{--        <div class="container-fluid padding">--}}
{{--            <div class="row text-center">--}}
{{--                <div class="col-sm-6 col-md-3">--}}
{{--                    <img class="gif" src="{{{asset('website/img/gif/panda.gif')}}}" alt="">--}}
{{--                </div>--}}
{{--                <div class="col-sm-6 col-md-3">--}}
{{--                    <img class="gif" src="{{{asset('website/img/gif/chicken.gif')}}}" alt="">--}}
{{--                </div>--}}
{{--                <div class="col-sm-6 col-md-3">--}}
{{--                    <img class="gif" src="{{{asset('website/img/gif/poo.gif')}}}" alt="">--}}
{{--                </div>--}}
{{--                <div class="col-sm-6 col-md-3">--}}
{{--                    <img class="gif" src="{{{asset('website/img/gif/unicorn.gif')}}}" alt="">--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

{{--    <!--- Welcome Section -->--}}
{{--    <div class="container-fluid padding">--}}
{{--        <div class="row welcome text-center">--}}
{{--            <div class="col-12">--}}
{{--                <h1 class="display-4">Main Categories:</h1>--}}
{{--            </div>--}}
{{--            <hr>--}}
{{--            <div class="col-12">--}}
{{--                <p class="lead" style="color: black">Welcome to The PetBite online shop next section we will intrduce you to our main categories as followed</p>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

{{--    <!--- Three Column Section -->--}}
{{--    <div class="container-fluid padding sec1">--}}
{{--        <div class="row text-center padding">--}}
{{--            <div class="col-xs-12 col-sm-6 col-md-3">--}}
{{--                <div class="pt-6">--}}
{{--                    <h3 style="color: black">Food</h3>--}}
{{--                    <P style="color: black">DRY/WET.</P>--}}
{{--                </div>--}}
{{--                <img src="{{{asset('website/img/imagea-svg.svg')}}}" class="img-fluid" alt="gi">--}}
{{--            </div>--}}
{{--            <div class="col-xs-12 col-sm-6 col-md-3">--}}

{{--                <div class="pt-6">--}}
{{--                    <h3 style="color: black">Treats and medicine</h3>--}}
{{--                    <P style="color: black">Make you animal happier.</P>--}}
{{--                </div>--}}
{{--                <img src="{{{asset('website/img/imagen.svg')}}}" class="img-fluid pb-2" alt="gi">--}}

{{--            </div>--}}
{{--            <div class="col-xs-12 col-sm-6 col-md-3">--}}
{{--                <div class="pt-6">--}}
{{--                    <h3 style="color: black">Games</h3>--}}
{{--                    <P style="color: black">gamesCloth</P>--}}
{{--                </div>--}}
{{--                <img src="{{{asset('website/img/imageg.svg')}}}" class="img-fluid pb-2" alt="gi">--}}

{{--            </div>--}}
{{--            <div class="col-xs-12 col-sm-6 col-md-3">--}}
{{--                <div class="">--}}
{{--                    <h3 style="color: black;">Cloth/Accesoiries</h3>--}}
{{--                    <P><h6 style="color:black;"></h6>clothes and accessoiries</P>--}}
{{--                </div>--}}
{{--                <img src="{{{asset('website/img/imageb.svg')}}}" class="img-fluid" alt="gi">--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <hr class="my-4">--}}
{{--    </div>--}}
{{--connect--}}
{{--    <div class="container-fluid paddin">--}}
{{--        <div class="row text-center padding">--}}
{{--            <div class="col-12">--}}
{{--                <h2>Connect</h2>--}}
{{--            </div>--}}
{{--            <div class="col-12 social padding pb-4">--}}
{{--                <ul class="social-network social-circle">--}}
{{--                    <li><a href="Facebook.com/tpetbite" class="icoFacebook" title="Facebook"><i class="fab fa-facebook"></i></a></li>--}}
{{--                    <li><a href="https://twitter.com/aliabdm" class="icoTwitter" title="Twitter"><i class="fab fa-twitter"></i></a></li>--}}
{{--                    <li><a href="Instagram.com/thepetbite" class="icoInstagram" title="instagram"><i class="fab fa-instagram"></i></a></li>--}}
{{--                </ul>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
    @endsection
