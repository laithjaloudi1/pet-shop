@extends('layouts.app')
@section('content')



    <!--- Two Column Section -->
    <div class="container-fluid padding pt-120">
        <div class="row padding">
            <div class="col-lg-6">
                <img src="{{{asset('website/img/petlogo.png')}}}" class="img-fluid" alt="gi">
            </div>
            <div class="col-lg-6" style="text-align: left;color: black" >
                <h2>The PetBite Shop...</h2>
                <p><b><h4>Who Are We?</h4></b><br>
                <p style="text-align: left;color: black">We are a small local online company in Jordan-Amman dedicated to providing families with:</p>
                <p style="text-align: left;color: black">food and supplies for dogs, cats, birds, and fish.  </p>
                <hr>
                <h2 style="text-align: left;color: black">Customer services...</h2>
                <p style="text-align: left;color: black"><b>What is that meaning to us?</b><br>
                    We value our clients’ opinions, recommendations and suggestions about the services that we provide. For that, we are always open to accepting new ideas and ventures that our customers want us to offer next. We take it a point to always listen to our clients for to us satisfy their whims and needs is the first and foremost thing that we should do. So, we encourage our clients to never hesitate to contact us. Clients can easily contact us through our Contact Us page.
                    ​.</p>

                <a href="{{route('contact-us')}}" class="btn  btn-lg pb-4 pt-4" style="background-color: #E4700F">contact us</a>
            </div>

        </div>

    </div>


    <div class="container-fluid">
        <div class="about">
            <h1 class="animated fadeInUp pt-3" style="text-align: center;">WHY ARE WE SPECIAL</h1>
            <h3 style="text-align: center;">What kind of Secial services we provide?</h3>
            <h6 style="text-align: center;color: black">If you are looking for quality animal products, then you are looking at the right direction</h6>
            <div class="row">
                <div class="col-md-4 crl1 about-grid animated fadeInLeft" data-wow-delay="1.4s">
                    <i class="fas fa-project-diagram" style="font-size: 50px;
          color: #fff;
          margin-bottom: 10px;"></i>
                    <h4>Sustainable service</h4>
                    <p>we will always be there to cater all your pet needs.
                    </p>

                </div>

                <div class="col-md-4 crl2 about-grid animated fadeInLeft" data-wow-delay="1s">
                    <i class="fas fa-chart-pie" style="font-size: 50px;
          color: #fff;
          margin-bottom: 10px;"></i>
                    <h4>Trustable services</h4>
                    <p>  We are the company that you can trust for your animal's needs.
                    </p>

                </div>
                <div class="col-md-4 crl3 about-grid animated fadeInLeft" data-wow-delay="0.6s">
                    <i class="fas fa-book-medical" style="font-size: 50px;
          color: #fff;
          margin-bottom: 10px;"></i>
                    <h4>Honest</h4>
                    <p>We promise to deliver you the kind of service that you and your dog truly deserve.
                    </p>

                </div>
            </div>
        </div>
    </div>



    <hr class="my-4">



    @endsection


