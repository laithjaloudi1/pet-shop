@extends('layouts.app')
@section('content')
    <div class="container pt-130">

        <table id="cart" class="table table-hover table-condensed">
            <thead>
            <tr>
                <th style="width:50%">Product</th>
                <th style="width:10%">Price</th>
                <th style="width:8%">Quantity</th>
                <th style="width:22%" class="text-center">Subtotal</th>
                <th style="width:10%"></th>
            </tr>
            </thead>
    @if($user->hasCart == null)
                <tbody>
                <tr>
                    <td>
                        <div class="row">
                            <div class="col-md-12">
                                <p class="text-center"><lapel>cart is empty</lapel></p>
                            </div>

                        </div>

                    </td>
                </tr>

                <tbody>
    @else

                    <tbody>
                    @foreach($user->hasCart->CartProduct as $cartproduct)
                    <tr>
                        <td data-th="Product">
                            <div class="row">
                                <div class="col-sm-2 hidden-xs">  <img class = 'imge' src="{{asset("storage/{$cartproduct->product->image}")}}" alt="product image" class="image-responsive" style="border-style:solid;border-width: 0.5px; margin-right: 19%;padding-top: 10%;width: 100%"/></div>
                                <div class="col-sm-10">
                                    <h4 class="nomargin">{{$cartproduct->title_en}}</h4>
                                    <p>{{$cartproduct->description_en}}</p>
                                </div>
                            </div>
                        </td>
                        <td data-th="Price" class="price{{$cartproduct->id}}">{{$cartproduct->price}}</td>
                        <td data-th="Quantity">
                            <input type="number"  onclick="change({{$cartproduct->id}})" class="form-control text-center total quantity{{$cartproduct->id}}" value="{{$cartproduct->quantity}}">
                        </td>
                        <td data-th="Subtotal" class="text-center tprice{{$cartproduct->id}}" >{{(double)$cartproduct->price* (double)$cartproduct->quantity}}</td>
                        <td class="actions" data-th="">
                            <form method="post" action="{{route('website_cart_destroy',$cartproduct->id)}}">
                                <input type="hidden" value="delete" name="_method">
                                @csrf
                                <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
                            </form>

                        </td>
                    </tr>
                    </tbody>
                @endforeach
                    <tfoot>
                    {{--<tr class="visible-xs">--}}
                        {{--<td class="text-center"><strong>Total </strong></td>--}}
                    {{--</tr>--}}
                    <tr>
                        <td><a href= "{{route('index')}}" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continue Shopping</a></td>
                        <td colspan="2" class="hidden-xs"></td>
                        <td class="hidden-xs text-center "><strong class="totalPrice">Total ${{$user->hasCart->totalPrice}}</strong></td>
                        <td><a href="{{route('website_cart_checkout',$user->hasCart->id)}}" class="btn btn-success btn-block">Checkout <i class="fa fa-angle-right"></i></a></td>
                    </tr>
                    </tfoot>





    @endif

        </table>
    </div>

    @endsection
<script>
    function change(id) {
        let price = $(`.price${id}`).html();
            let value = $(`.quantity${id}`).val();
            price = parseFloat(price,10);
        $(`.tprice${id}`).html(price*value);

        $.ajax({
            type:'POST',
            url:`/change-quantity/${id}`,
            data: {
                "_token": "{{ csrf_token() }}",
                "quantity": value
            },
            success:function(data) {
               //console.log(data.total);
                $('.totalPrice').html("Total $"+data.total)
            }
        });
    }
</script>
