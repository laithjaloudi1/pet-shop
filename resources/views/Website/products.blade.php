@extends('layouts.app')
@section('content')

    <h1>Product List</h1>
    <div class="container-fluid padding pt-120">

        <form method="get" action="{{route('website_products',$category_id)}}?type={{$type}}">
            <div class="form-row">
                <div class="form-group col-md-4 ">
                    <input hidden name="type" value="{{$type}}">
                    <input type="text" class="form-control" id="search" name="search"
                           value="{{Request::get('search','')}}" placeholder="Search...">
                </div>
                <div class="form-group col-md-4 ">
                    <select name="order" id="order" class="form-control">
                        <option value="order" {{Request::get('order','created_at')=='order'?'selected':''}}>Sorted by
                            order
                        </option>
                        <option value="created_at" {{Request::get('order','created_at')=='created_at'?'selected':''}}>
                            Sorted by date
                        </option>
                    </select>
                </div>

                <div class="form-group col-md-6 ">
                    <select name="brand" id="order" class="form-control">
                        <option value="">select brand</option>
                        @foreach($brands as $brand)
                        <option value="{{$brand->id}} ">{{$brand->title_en}}
                        </option>
                            @endforeach
                    </select>
                </div>
                <div class="form-group col-md-2">
                    <button type="submit" class="btn btn-info">Filter</button>

                </div>
            </div>
        </form>
        <div class="row padding">
            @foreach($products as $product)
                <div class="col-md-4">
                    <div class="product-card">
                        <div class="product-image">
                            <img class="card-img-top imge" src="{{asset("storage/{$product->image}")}}" alt="product image">
                        </div>
                        <div class="product-info">
                            <h4 class="card-title">{{$product->title_en}}</h4>
                            <h6 class="card-text">{{$product->price}}JD</h6>
                            <a href="{{route('website_product',$product->id)}}"
                               class="btn btn-outline-secondary">Show</a>

                        </div>
                    </div>
                </div>
            @endforeach

        </div>

    </div>


@endsection
