@extends('layouts.app')
@section('content')

    <div class="container bg-light">
        <div class="py-5 text-center">
            <img class="d-block mx-auto mb-4" src="" alt="" width="72" height="72">
            <h2>Checkout form</h2>
            <p class="lead">check out for your cart.</p>
        </div>

        <div class="row">
            <div class="col-md-4 order-md-2 mb-4">
                <h4 class="d-flex justify-content-between align-items-center mb-3">
                    <span class="text-muted">Your cart</span>
                    <span class="badge badge-secondary badge-pill">product count {{$cart->CartProduct()->count()}}</span>
                </h4>
                <ul class="list-group mb-3">
                    @foreach($cart->CartProduct as $item)
                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                        <div>
                            <h6 class="my-0">{{$item->product->title_en}}</h6>
                            <small class="text-muted">{{$item->product->description_en}}</small>
                        </div>

                        <span class="text-muted">${{$item->product->Price($item->id)}}</span>
                    </li>
                    @endforeach
                    <li class="list-group-item d-flex justify-content-between bg-light">
                        <div class="text-success">
                            <h6 class="my-0">Promo code</h6>
                            <small>EXAMPLECODE</small>
                        </div>
                        <span class="text-success">-$0</span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between">
                        <span>Total (JOD)</span>
                        <strong>${{$cart->totalPrice}}</strong>
                    </li>
                </ul>

                <form class="card p-2">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Promo code">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-secondary">Redeem</button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-md-8 order-md-1">
                <h4 class="mb-3">Billing address</h4>
                <form class="needs-validation" action="{{route('website_cart_checkout',$cart->id)}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="firstName">First name</label>
                                <input type="text" class="form-control" name="first_name" id="firstName" placeholder="first name" required>
                                <div class="invalid-feedback">
                                    Valid first name is required.
                                </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="lastName">Last name</label>
                            <input type="text" class="form-control" id="lastName" name="last_name" placeholder="last name" required>
                            <div class="invalid-feedback">
                                Valid last name is required.
                            </div>
                        </div>
                    </div>

                    {{--<div class="mb-3">--}}
                        {{--<label for="username">Username</label>--}}
                        {{--<div class="input-group">--}}
                            {{--<div class="input-group-prepend">--}}
                                {{--<span class="input-group-text">@</span>--}}
                            {{--</div>--}}
                            {{--<input type="text" class="form-control" id="username" placeholder="Username" required>--}}
                            {{--<div class="invalid-feedback" style="width: 100%;">--}}
                                {{--Your username is required.--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <div class="mb-3">
                        <label for="email">Email <span class="text-muted"></span></label>
                        <input type="email" class="form-control" name="email" id="email" placeholder="you@example.com" required>
                        <div class="invalid-feedback">
                            Please enter a valid email address for shipping updates.
                        </div>
                    </div>

                    <div class="mb-3">
                        <label for="address">country</label>
                        <input type="text" class="form-control" name="country" id="address" placeholder="1234 Main St" required>
                        <div class="invalid-feedback">
                            Please enter your shipping address.
                        </div>
                    </div>

                    <div class="mb-3">
                        <label for="address">city</label>
                        <input type="text" class="form-control" name="city" id="address" placeholder="1234 Main St" required>
                        <div class="invalid-feedback">
                            Please enter your shipping address.
                        </div>
                    </div>


                    <div class="row">
                        <div class="mb-3">
                            <label for="address">street</label>
                            <input type="text" class="form-control" name="street" id="address" placeholder="1234 Main St" required>
                            <div class="invalid-feedback">
                                Please enter your shipping address.
                            </div>
                        </div>


                        <div class="mb-3 ml-1">
                            <label for="address">block</label>
                            <input type="text" class="form-control" id="address" name="block" placeholder="1234 Main St" required>
                            <div class="invalid-feedback">
                                Please enter your shipping address.
                            </div>
                        </div>

                        <div class="mb-3">
                            <label for="address">apartment</label>
                            <input type="text" class="form-control" name="apartment_number" id="address" placeholder="1234 Main St" required>
                            <div class="invalid-feedback">
                                Please enter your shipping address.
                            </div>
                        </div>
                        <div class="mb-3 ml-1">
                            <label for="email">Phone Number <span class="text-muted"></span></label>
                            <input type="text" class="form-control" name="phone_number" id="email" placeholder="+9627811525" required>
                            <div class="invalid-feedback">
                                Please enter your phone number
                            </div>
                        </div>
                    </div>
                        <hr class="mb-4">
                        <button class="btn btn-primary btn-lg btn-block"  type="submit">checkout</button>
                </form>
            </div>
        </div>
    </div>



@endsection

