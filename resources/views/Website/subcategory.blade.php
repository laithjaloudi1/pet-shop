@extends('layouts.app')
@section('content')
    <div class="container-fluid pt-120">

            <section>
                <img class="se imge" id="logo" src="{{asset("storage/{$subCategory->image}")}}">
                <h1>{{$subCategory->title_en}}</h1>
                <p class="subhead">{{$subCategory->description_en}}</p>
            </section>

    </div>


    <!--- Cards -->
    <div class="container-fluid padding pb-5 pt-2">
        <div class="row padding">
            @foreach($hastypes as $hastype)
                <div class="col-xl-4 pt-2 pb-2">
                    <div class="">
                        <a  href="{{route('website_products',$hastype->id)}}?type=1" class="btn" >  <img class="imge" src="{{asset("storage/{$hastype->image}")}}" alt="vsc" style=" border-radius: 5%"></a>
                        <div class="card-body">
                            <h4 class="card-title">{{$hastype->title_en}}</h4>
                            <p class="card-text">{{$hastype->description_en}}</p>
                                <a  href="{{route('website_products',$hastype->id)}}?type=1" class="btn" style="background-color: white;color: black;border: 2px solid #fc9403; " >{{$hastype->title_en}}</a>
                    </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
