@extends('layouts.app')
@section('content')
    <div class="container-fluid pt-120">
        <section>
{{--            <img class="se imge" id="logo" src="{{asset("storage/{$category->image}")}}">--}}
            <h1>{{$category->title_en}}</h1>
            <p class="subhead">{{$category->description_en}}</p>
        </section>
    </div>


    <!--- Cards -->
    <div class="container-fluid padding pb-5 pt-2">
        <div class="row padding">
            @foreach($subCategories as $subCat)
            <div class="col-xl-4 pt-2 pb-2">
                <div class="">
                    @if($subCat->has_type == 0)
                        <a href="{{route('website_products',$subCat->id)}}?type=0">
                            <img class=" imge" src="{{asset("storage/{$subCat->image}")}}" alt="vsc" style=" border-radius: 5%">
                        </a>
                    @else
                        <a href="{{route('website_subcategory',$subCat->id)}}" >
                            <img class=" imge" src="{{asset("storage/{$subCat->image}")}}" alt="vsc" style=" border-radius: 5%">
                        </a>
                    @endif
                    <div class="card-body">
                        <h4 class="card-title">{{$subCat->title_en}}</h4>
                        <p class="card-text">{{$subCat->description_en}}</p>
                        @if($subCat->has_type == 0)
                            <a href="{{route('website_products',$subCat->id)}}?type=0" class="btn" style="background-color: white;color: black;border: 2px solid #fc9403; " >{{$subCat->title_en}}</a>
                            @else
                            <a href="{{route('website_subcategory',$subCat->id)}}" class="btn" style="background-color: white;color: black;border: 2px solid #fc9403; " >{{$subCat->title_en}}</a>
                            @endif
                    </div>
                </div>
            </div>
                @endforeach

        </div>
    </div>
    @endsection
