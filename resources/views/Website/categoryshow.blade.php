@extends('layouts.app')
@section('content')
    <div class="container-fluid pt-120">
        <section>
            {{--            <img class="se imge" id="logo" src="{{asset("storage/{$category->image}")}}">--}}
            <h1>Shop Now</h1>
            <p class="subhead">Our Categories</p>
        </section>
    </div>


    <!--- Cards -->
    <div class="container-fluid padding pb-5 pt-2">
        <div class="row padding">
            @foreach($categories as $category)
                <div class="col-xl-4 pt-2 pb-2">
                    <div class="">
                            <a href="{{route('website_category',$category->id)}}?type=0">
                                <img class=" imge" src="{{asset("storage/{$category->image}")}}" alt="vsc" style=" border-radius: 5%">
                            </a>
                        <div class="card-body">
                            <h4 class="card-title">{{$category->title_en}}</h4>
                            <p class="card-text">{{$category->description_en}}</p>
                                <a href="{{route('website_category',$category->id)}}" class="btn" style="background-color: white;color: black;border: 2px solid #fc9403; " >{{$category->title_en}}</a>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
@endsection
