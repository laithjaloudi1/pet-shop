<script type="text/javascript" src="{{asset('js/vendor.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bundle.js')}}"></script>

@stack('scripts')


<!-- include libraries(jQuery, bootstrap) -->
{{--<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">--}}


<script type="text/javascript">
    $(document).ready( function() {
        $("#description_en").summernote({
            height: 200,
            popover: {
                image: [],
                link: [],
                air: []
            }
        });
        $("#description_ar").summernote({
            height: 200,
            popover: {
                image: [],
                link: [],
                air: []
            }
        });
    });

    $(document).ready(function () {
        $('.products').select2();
    })
</script>

