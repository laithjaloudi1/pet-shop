<?php
/**
 * Created by PhpStorm.
 * User: dura
 * Date: 11/28/18
 * Time: 1:41 PM
 */
/** @var array $message */
?>
<!-- Custom Theme JavaScript -->
{{--array check to make sure it is not message model--}}
@if(Session::has('sessionMessage') || isset($message) && is_array($message))
<?php $message = Session::get('sessionMessage') ?: $message?>
        <script>
            $(function() {
                "use strict";
                $.toast({
                    heading: '{{$message['type']}}',
                    text: '{{ $message['message'] }}',
                    position: 'top-right',
                    @if($message['type'] == "danger")
                    bgColor : 'red',
                    @else
                    bgColor : 'green',
                    @endif
                    icon: '{{$message['type']}}',
                    hideAfter: 3000,
                    stack: 6
                });
            });
        </script>
@endif
