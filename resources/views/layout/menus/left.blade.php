<div class="sidebar">
    <div class="sidebar-inner">
        <!-- ### $Sidebar Header ### -->
        <div class="sidebar-logo">
            <div class="peers ai-c fxw-nw">
                <div class="peer peer-greed">
                    <a class="sidebar-link td-n" href="/home">
                        <div class="peers ai-c fxw-nw">
                            <div class="peer">
                                <div class="logo">
                                    <img src="/static/images/logo.jpg" style="width: 37px; margin: 14px" alt="">
                                </div>
                            </div>
                            <div class="peer peer-greed">
                                <h5 class="lh-1 mB-0 logo-text">Administrator</h5>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="peer">
                    <div class="mobile-toggle sidebar-toggle">
                        <a href="" class="td-n">
                            <i class="ti-arrow-circle-left"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <!-- ### $Sidebar Menu ### -->
        <ul class="sidebar-menu scrollable pos-r">
            <li class="nav-item">
                <a class="sidebar-link" href="{{route('categories.index')}}">
                <span class="icon-holder">
                  <i class="fa fa-list"></i>
                </span>
                    <span class="title">Categories</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="sidebar-link" href="{{route('sub-categories.index')}}">
                <span class="icon-holder">
                  <i class="fa fa-list"></i>
                </span>
                    <span class="title">Sub Categories</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="sidebar-link" href="{{route('has-types.index')}}">
                <span class="icon-holder">
                  <i class="fa fa-list"></i>
                </span>
                    <span class="title">Sub Categories types</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="sidebar-link" href="{{route('brands.index')}}">
                <span class="icon-holder">
                  <i class="fa fa-list"></i>
                </span>
                    <span class="title">Brands</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="sidebar-link" href="{{route('products.index')}}">
                <span class="icon-holder">
                  <i class="fa fa-list"></i>
                </span>
                    <span class="title">Products</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="sidebar-link" href="{{route('admin.index')}}">
                <span class="icon-holder">
                  <i class="fa fa-list"></i>
                </span>
                    <span class="title">Admin</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="sidebar-link" href="{{route('users.index')}}">
                <span class="icon-holder">
                  <i class="fa fa-list"></i>
                </span>
                    <span class="title">Users</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="sidebar-link" href="{{route('orders.index')}}">
                <span class="icon-holder">
                  <i class="fa fa-list"></i>
                </span>
                    <span class="title">Orders</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="sidebar-link" href="{{route('delivered.index')}}">
                <span class="icon-holder">
                  <i class="fa fa-list"></i>
                </span>
                    <span class="title">delivered</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="sidebar-link" href="{{route('cancel.index')}}">
                <span class="icon-holder">
                  <i class="fa fa-list"></i>
                </span>
                    <span class="title">cancel</span>
                </a>
            </li>

        </ul>
    </div>
</div>
