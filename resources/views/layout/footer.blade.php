<!-- ### $App Screen Footer ### -->
<footer class="bdT ta-c p-30 lh-0 fsz-sm c-grey-600">
    <span>Copyright © {{date('Y')}} Developed by <a href="https://www.linkedin.com/in/m-ali-abdulwahed-1533b9171/" target='_blank' title="Colorlib">M Ali Abdulwahed</a>. All rights reserved.</span>
</footer>
