@extends('layout.app')
@section('content')
    <div class="masonry-item col-md-6">
        <div class="bgc-white p-20 bd">
            <h6 class="c-grey-900">{{trans('SubCategory.create')}}</h6>
            <form action="{{route('sub-categories.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="device_id" id="device_id"  name="device_id" >
                <div class="form-group">
                    <label for="username">{{trans('SubCategory.title_en')}}</label>
                    <input type="text" class="form-control" id="title_en"  name="title_en" placeholder="Enter Title " required>
                </div>
                <div class="form-group">
                    <label for="username">{{trans('SubCategory.title_ar')}}</label>
                    <input type="text" class="form-control" id="username"  name="title_ar" placeholder="Enter Title " required>
                </div>
                <div class="form-group">
                    <label for="username">{{trans('SubCategory.description_en')}}</label>
                    <input type="text" class="form-control" id="username"  name="description_en" placeholder="Enter description " required>
                </div>
                <div class="form-group">
                    <label for="username">{{trans('SubCategory.description_ar')}}</label>
                    <input type="text" class="form-control" id="username"  name="description_ar" placeholder="Enter description" required>
                </div>

                <div class=" form-group ">
                    <label for="price">{{trans('SubCategory.category')}}</label>
                    <select name="category_id" class="form-control pb-2" id="category_id"  style="width: 100%" >
                        @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->title_en}}</option>
                        @endforeach
                    </select>
                </div>

                    <fieldset class="form-group">
                        <div class="row">
                            <label class="col-form-legend col-sm-2">{{trans('products.status')}}</label>
                            <div class="col-sm-10 pl-5">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="has_type" id="gridRadios1" value="1" checked>
                                        {{trans('hastype.active')}}
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="has_type" id="gridRadios2" value="0">
                                        {{trans('hastype.closed')}}
                                    </label>
                                </div>
                            </div>
                        </div>
                    </fieldset>

                <div class="form-group mb-5 custom-file">
                    <label for="image" class="custom-file-label ">{{trans('SubCategory.image')}}</label>
                    <input type="file" class="form-control-file custom-file-input" id="image" name="file" placeholder="Upload Image" required>
                </div>

                <button type="submit" class="btn btn-primary">{{trans('SubCategory.submit')}}</button>
            </form>
        </div>
    </div>
    <script>

    </script>
@endsection
