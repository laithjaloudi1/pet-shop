@extends('layout.app')
@section('content')
    <div class="masonry-item col-md-6">
        <div class="bgc-white p-20 bd">
            <h6 class="c-grey-900">{{trans('users.create')}}</h6>
            <form action="{{route('users.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="device_id" id="device_id"  name="device_id" >
                <div class="form-group">
                    <label for="username">{{trans('users.name')}}</label>
                    <input type="text" class="form-control" id="username"  name="name" placeholder="Enter Title " required>
                </div>
                <div class="form-group">
                    <label for="username">{{trans('users.email')}}</label>
                    <input type="text" class="form-control" id="username"  name="email" placeholder="Enter Title " required>
                </div>
                <div class="form-group">
                    <label for="username">{{trans('users.password')}}</label>
                    <input type="text" class="form-control" id="username"  name="password" placeholder="Enter description " required>
                </div>


                <button type="submit" class="btn btn-primary">{{trans('users.submit')}}</button>
            </form>
        </div>
    </div>
    <script>

    </script>
@endsection
