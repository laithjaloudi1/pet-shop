<?php
/**
 * @var \App\Entities\Users\User[] $data
 */
?>
@extends('layout.app')
@section('content')
    <h4 class="c-grey-900 mT-10 mB-30">{{trans('brands.info')}}</h4>
    <div class="row">
        <div class="col-md-12">
            <div class="masonry-item col-12">
                <div class="bd bgc-white">
                    <div class="peers fxw-nw@lg+ ai-s">
                        <div class="peer peer-greed w-60p@lg+ w-100@lg- p-20">
                            <div class="layers">
                                <div class="layer w-100">
                                    <div class="img-fluid">
                                        <img style="width: 80%;"
                                             src="{{asset("storage/{$item->image}")}}"
                                             alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="peer bdL p-20 w-40p@lg+ w-100p@lg-">
                            <div class="layers">
                                <div class="layer w-100">
                                    <div class="layers">
                                        <div class="layer w-100">
                                            <h5 class="mB-5"></h5>
                                        </div>
                                    </div>
                                    <div class=" pT-20 mT-20 bdT fxw-nw@lg+ jc-sb">
                                        <table class="table table-bordered">
                                            <tr>
                                                <td>{{trans('brands.title_en')}}</td>
                                                <td>{{$item->title_en}}</td>
                                            </tr>
                                            <tr>
                                                <td>{{trans('brands.title_ar')}}</td>
                                                <td>{{$item->title_ar}}</td>
                                            </tr>
                                             <tr>
                                                <td>{{trans('brands.description_en')}}</td>
                                                <td>{{$item->description_en}}</td>
                                            </tr>
                                             <tr>
                                                <td>{{trans('brands.description_ar')}}</td>
                                                <td>{{$item->description_ar}}</td>
                                            </tr>
                                            <tr>
                                                <td>{{trans('brands.created_by')}}</td>
                                                <td>{{$item->admin->name}}</td>
                                            </tr>
                                            <tr>
                                                <td>{{trans('brands.created_at')}}</td>
                                                <td>{{$item->created_at}}</td>
                                            </tr>
                                            <tr>
                                                <td>{{trans('brands.update_at')}}</td>
                                                <td>{{$item->updated_at}}</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <a href="{{route('brands.index')}}" class="btn btn-secondary">{{trans('brands.back')}}</a>
                            <a href="{{route('brands.edit',$item->id)}}" class="btn btn-info pull-right">{{trans('brands.edit')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
