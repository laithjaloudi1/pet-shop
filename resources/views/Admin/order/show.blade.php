<?php
/**
 * @var \App\Entities\Users\User[] $data
 */
?>
@extends('layout.app')
@section('content')
    <h4 class="c-grey-900 mT-10 mB-30">{{trans('order.info')}}</h4>
    <div class="row">
        <div class="col-md-12">
            <div class="masonry-item col-12">
                <div class="bd bgc-white">
                    <div class="peers fxw-nw@lg+ ai-s">
                        <div class="peer peer-greed w-60p@lg+ w-100@lg- p-20">
                            <div class="layers">
                                <div class="layer w-100">
                                    <table class="table table-bordered">
                                        @foreach($item->CartProduct as $product)
                                        <tr>
                                            <td>{{trans('order.name')}}</td>
                                            <td>{{$product->product->title_en}}</td>
                                        </tr>

                                            <tr>
                                                <td>{{trans('order.quantity')}}</td>
                                                <td>{{$product->quantity}}</td>
                                            </tr>
                                            <tr>
                                                <td>{{trans('order.price')}}</td>
                                                <td>{{$product->product->price}}</td>
                                            </tr>
                                            <hr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="peer bdL p-20 w-40p@lg+ w-100p@lg-">
                            <div class="layers">
                                <div class="layer w-100">
                                    <div class="layers">
                                        <div class="layer w-100">
                                            <h5 class="mB-5"></h5>
                                        </div>
                                    </div>
                                    <div class=" pT-20 mT-20 bdT fxw-nw@lg+ jc-sb">
                                        <table class="table table-bordered">
                                            <tr>
                                                <td>{{trans('Buyer')}}</td>
                                                <td>{{$item->first_name}} {{$item->last_name}}</td>
                                            </tr>
                                            <tr>
                                                <td>{{trans('Buyer number')}}</td>
                                                <td>{{$item->phone_number}}</td>
                                            </tr>
                                            <tr>
                                                <td>{{trans('Buyer email')}}</td>
                                                <td>{{$item->email}}</td>
                                            </tr>
                                             <tr>
                                                <td>{{trans('Buyer country')}}</td>
                                                <td>{{$item->country}}</td>
                                            </tr>
                                             <tr>
                                                <td>{{trans('Buyer city')}}</td>
                                                <td>{{$item->city}}</td>
                                            </tr>

                                            <tr>
                                                <td>{{trans('Buyer street')}}</td>
                                                <td>{{$item->street}}</td>
                                            </tr>
                                            <tr>
                                                <td>{{trans('Buyer block')}}</td>
                                                <td>{{$item->block}}</td>
                                            </tr>

                                            <tr>
                                                <td>{{trans('Buyer apartment_number')}}</td>
                                                <td>{{$item->apartment_number}}</td>
                                            </tr>

                                            <tr>
                                                <td>{{trans('order totalPrice')}}</td>
                                                <td>{{$item->totalPrice}}</td>
                                            </tr>

                                        </table>
                                    </div>
                                </div>
                            </div>
{{--                            <a href="{{route('has-types.index')}}" class="btn btn-secondary">{{trans('has-type.back')}}</a>--}}
{{--                            <a href="{{route('has-types.edit',$item->id)}}" class="btn btn-info pull-right">{{trans('has-type.edit')}}</a>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
