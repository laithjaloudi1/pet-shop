@extends('layout.app')
@section('content')
    <div class="masonry-item col-md-6">
        <div class="bgc-white p-20 bd">
            <h6 class="c-grey-900">{{trans('products.create')}}</h6>
            <form action="{{route('products.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="device_id" id="device_id"  name="device_id" >
                <div class="form-group">
                    <label for="username">{{trans('products.title_en')}}</label>
                    <input type="text" class="form-control" id="username"  name="title_en" placeholder="Enter Title " required>
                </div>
                <div class="form-group">
                    <label for="username">{{trans('products.title_ar')}}</label>
                    <input type="text" class="form-control" id="username"  name="title_ar" placeholder="Enter Title " required>
                </div>
                <div class="form-group">
                    <label for="username">{{trans('products.description_en')}}</label>
                    <input type="text" class="form-control" id="username"  name="description_en" placeholder="Enter description " required>
                </div>
                <div class="form-group">
                    <label for="username">{{trans('products.description_ar')}}</label>
                    <input type="text" class="form-control" id="username"  name="description_ar" placeholder="Enter description" required>
                </div>

                <div class="form-group">
                    <label for="username">{{trans('products.price')}}</label>
                    <input type="number" min="0" step="0.01" class="form-control" id="price"  name="price" placeholder="Enter price">
                </div>

                <div class=" form-group ">
                    <label for="price">{{trans('products.brand')}}</label>
                    <select name="brand_id" class="form-control pb-2" id="brand_id"  style="width: 100%" >
                        @foreach($brands as $brand)
                            <option value="{{$brand->id}}">{{$brand->title_en}}</option>
                        @endforeach
                    </select>

                <fieldset class="form-group">
                    <div class="row">
                        <label class="col-form-legend col-sm-2">{{trans('products.status')}}</label>
                        <div class="col-sm-10 pl-5">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" onclick="change(1)"  name="has_type" id="has-type" value="1" >
{{--                                    {{trans('products.active')}}--}}
                                    has types

                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="radio" onclick="change(0)" name="has_type" id="has-not-type" value="0">
{{--                                    {{trans('products.closed')}}--}}
                                    dose not has type
                                </label>
                            </div>
                        </div>
                    </div>
                </fieldset>

                <div class=" form-group " id="sub_cat">
                    <label for="price">{{trans('products.sub-category')}}</label>
                    <select name="sub_category" class="form-control pb-2" id="brand_id"  style="width: 100%" >
                        @foreach($subCategories as $subCat)
                            <option value="{{$subCat->id}}">{{$subCat->title_en}}</option>
                        @endforeach
                    </select>
                </div>

                <div class=" form-group " id="sub_cat_type">
                    <label for="price">{{trans('products.sub-category_type')}}</label>
                    <select name="sub_category_types" class="form-control pb-2" id="brand_id"  style="width: 100%" >
                        @foreach($subCategoriesTypes as $subCatType)
                            <option value="{{$subCatType->id}}">{{$subCatType->title_en}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group mb-5 custom-file">
                    <label for="image" class="custom-file-label">{{trans('products.image')}}</label>
                    <input type="file" class="form-control-file custom-file-input" id="image" name="file" placeholder="Upload Image" required>
                </div>

                <button type="submit" class="btn btn-primary">{{trans('products.submit')}}</button>
            </form>
        </div>
    </div>
    <script>
        $(document).ready(function () {
           $('#sub_cat').hide();
           $('#sub_cat_type').hide();
        });
        function change(value) {
            if (value === 1)
            {
                $('#sub_cat_type').show();
                $('#sub_cat').hide();

            }else{
                $('#sub_cat').show();
                $('#sub_cat_type').hide();
            }
        }
    </script>
@endsection
