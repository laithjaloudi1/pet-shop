@extends('layout.app')
@section('content')

    <div id='mainContent'>
        <div class="container-fluid">
            <h4 class="c-grey-900 mT-10 mB-30">{{trans('admins.admins')}}</h4>
            <div class="row">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20 mB-20">
                        <h4 class="c-grey-900 mB-20">{{trans('admins.list')}}
                            <a href="{{route('admin.create')}}"
                               class="btn btn-success btn-sm pull-right">{{trans('admins.create')}}</a>
                        </h4>
                        {{--<form>--}}
                        {{--<div class="form-row">--}}
                        {{--<div class="form-group col-md-4 ">--}}
                        {{--<input type="text" class="form-control" id="search" name="search"--}}
                        {{--value="{{Request::get('search','')}}" placeholder="Search...">--}}
                        {{--</div>--}}
                        {{--<div class="form-group col-md-4 ">--}}
                        {{--<select name="order" id="order" class="form-control">--}}
                        {{--                                        <option value="order" {{Request::get('order','created_at')=='order'?'selected':''}}>Sorted by order</option>--}}
                        {{--<option value="created_at" {{Request::get('order','created_at')=='created_at'?'selected':''}}>Sorted by date</option>--}}
                        {{--</select>--}}
                        {{--</div>--}}
                        {{--<div class="form-group">--}}
                        {{--<fieldset class="form-group">--}}
                        {{--<div class="row">--}}
                        {{--<div class="col-sm-12 form-check-inline">--}}
                        {{--<div class="form-check">--}}
                        {{--<label class="form-check-label form-check-inline">--}}
                        {{--<input class="form-check-input" type="radio" name="direction"--}}
                        {{--id="direction-ascending" value="asc" {{Request::get('direction','desc')=='asc'?'checked':''}}>--}}
                        {{--{{trans('advertisements.ascending')}}--}}
                        {{--</label>--}}
                        {{--</div>--}}
                        {{--<div class="form-check">--}}
                        {{--<label class="form-check-label form-check-inline">--}}
                        {{--<input class="form-check-input" type="radio" name="direction"--}}
                        {{--id="direction-descending" value="desc" {{Request::get('direction','desc')=='desc'?'checked':''}}>--}}
                        {{--{{trans('advertisements.descending')}}--}}
                        {{--</label>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</fieldset>--}}
                        {{--</div>--}}
                        {{--<div class="form-group col-md-10">--}}
                        {{--<fieldset class="form-group">--}}
                        {{--<div class=" form-check-inline">--}}
                        {{--<label class="col-form-legend form-check-label">{{trans('users.status')}}: </label>--}}
                        {{--<div class="col-sm-10 form-check-inline">--}}
                        {{--<div class="form-check">--}}
                        {{--<label class="form-check-label form-check-inline">--}}
                        {{--<input class="form-check-input" type="radio" name="status"--}}
                        {{--id="direction-ascending" value="0" {{Request::get('status','')=='0'?'checked':''}}>--}}
                        {{--{{trans('users.suspend')}}--}}
                        {{--</label>--}}
                        {{--</div>--}}
                        {{--<div class="form-check">--}}
                        {{--<label class="form-check-label form-check-inline">--}}
                        {{--<input class="form-check-input" type="radio" name="status"--}}
                        {{--id="direction-descending" value="1" {{Request::get('status','')=='1'?'checked':''}}>--}}
                        {{--{{trans('users.active')}}--}}
                        {{--</label>--}}
                        {{--</div>--}}
                        {{--<div class="form-check">--}}
                        {{--<label class="form-check-label form-check-inline">--}}
                        {{--<input class="form-check-input" type="radio" name="status"--}}
                        {{--id="direction-descending" value="" {{Request::get('status','')==''?'checked':''}}>--}}
                        {{--{{trans('users.both')}}--}}
                        {{--</label>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</fieldset>--}}
                        {{--</div>--}}
                        {{--<div class="form-group col-md-2">--}}
                        {{--<button type="submit" class="btn btn-info">Filter</button>--}}
                        {{--<a href="{{route('production.index')}}"  class="btn btn-outline-secondary">reset</a>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</form>--}}

                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">{{trans('admins.id')}}</th>
                                <th scope="col">{{trans('admins.name')}}</th>
                                <th scope="col">{{trans('admins.email')}}</th>
                                <th scope="col">{{trans('admins.password')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($data as $item)
                                <tr>
                                    <th scope="row">{{$item->id}}</th>
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->email}}</td>
                                    <td>{{$item->password}}</td>
                                    <td>
{{--                                        <a href="{{route('categories.show',$item->id)}}" class="btn btn-info btn-block btn-sm">{{trans('categories.show')}}</a>--}}
{{--                                        <br>--}}
{{--                                        <a href="{{route('categories.edit',$item->id)}}" class="btn btn-secondary btn-block btn-sm">{{trans('categories.edit')}}</a>--}}
{{--                                        <br>--}}
                                        <form method="post" action="{{route('admin.destroy',$item->id)}}">
                                            <input type="hidden" value="delete" name="_method">
                                            @csrf
                                            <button type="submit" class="btn btn-danger btn-block btn-sm">{{trans('admin.delete')}}</button>
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                <tr><td class="text-center" colspan="8">Empty</td></tr>
                            @endforelse
                            </tbody>
                        </table>
                                                {{$data->appends(Request::all())->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
