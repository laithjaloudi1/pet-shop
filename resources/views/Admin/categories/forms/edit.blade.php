@extends('layout.app')
@section('content')
    <div class="masonry-item col-md-6">
        <div class="bgc-white p-20 bd">
            <h6 class="c-grey-900">{{trans('categories.edit')}}</h6>
            <form action="{{route('categories.update',$item->id)}}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="device_id" id="device_id"  name="device_id" >
                <input type="hidden" value="PUt" name="_method">
                <div class="form-group">
                    <label for="username">{{trans('categories.title_en')}}</label>
                    <input type="text" class="form-control" id="username" value="{{$item->title_en}}" name="title_en" placeholder="Enter Title " required>
                </div>
                <div class="form-group">
                    <label for="username">{{trans('categories.title_ar')}}</label>
                    <input type="text" class="form-control" id="username" value="{{$item->title_ar}}"   name="title_ar" placeholder="Enter Title " required>
                </div>
                <div class="form-group">
                    <label for="username">{{trans('categories.description_en')}}</label>
                    <input type="text" class="form-control" id="username" value="{{$item->description_en}}"   name="description_en" placeholder="Enter description " required>
                </div>
                <div class="form-group">
                    <label for="username">{{trans('categories.description_ar')}}</label>
                    <input type="text" class="form-control" id="username" value="{{$item->description_ar}}"   name="description_ar" placeholder="Enter description" required>
                </div>
                <div class="form-group mb-5 custom-file">
                    <label for="image" class="custom-file-label">{{trans('categories.image')}}</label>
                    <input type="file" class="form-control-file custom-file-input" id="image" name="file" placeholder="Upload Image" >
                </div>

                <button type="submit" class="btn btn-primary">{{trans('users.submit')}}</button>
            </form>
        </div>
    </div>
    <script>

    </script>
@endsection
