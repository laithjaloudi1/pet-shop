@extends('layout.app')
@section('content')
    <div class="masonry-item col-md-6">
        <div class="bgc-white p-20 bd">
            <h6 class="c-grey-900">{{trans('has-types.edit')}}</h6>
            <form action="{{route('has-types.update',$item->id)}}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="device_id" id="device_id"  name="device_id" >
                <input type="hidden" value="PUt" name="_method">
                <div class="form-group">
                    <label for="username">{{trans('has-types.title_en')}}</label>
                    <input type="text" class="form-control" id="username" value="{{$item->title_en}}" name="title_en" placeholder="Enter Title " required>
                </div>
                <div class="form-group">
                    <label for="username">{{trans('has-types.title_ar')}}</label>
                    <input type="text" class="form-control" id="username" value="{{$item->title_ar}}"   name="title_ar" placeholder="Enter Title " required>
                </div>
                <div class="form-group">
                    <label for="username">{{trans('has-types.description_en')}}</label>
                    <input type="text" class="form-control" id="username" value="{{$item->description_en}}"   name="description_en" placeholder="Enter description " required>
                </div>
                <div class="form-group">
                    <label for="username">{{trans('has-types.description_ar')}}</label>
                    <input type="text" class="form-control" id="username" value="{{$item->description_ar}}"   name="description_ar" placeholder="Enter description" required>
                </div>
                <div class=" form-group ">
                    <label for="price">{{trans('has-types.category')}}</label>
                    <select name="sub_category_id" class="form-control pb-2" id="sub_category_id"  style="width: 100%" >
                        @foreach($subcategories as $subcategory)
                            @if($item->category_id == $subcategory->id)
                                <option selected value="{{$subcategory->id}}">{{$subcategory->title_en}}</option>
                                @else
                                <option value="{{$subcategory->id}}">{{$subcategory->title_en}}</option>

                            @endif
                        @endforeach
                    </select>
                </div>

                <fieldset class="form-group">
                    <div class="row">
                        <label class="col-form-legend col-sm-2">{{trans('products.status')}}</label>
                        <div class="col-sm-10 pl-5">
                            <div class="form-check">
                                <label class="form-check-label">
                                    @if($item->has_type == 1)
                                    <input  class="form-check-input" type="radio" name="has_type" id="gridRadios1" value="1" checked>
                                    @else
                                        <input  class="form-check-input" type="radio" name="has_type" id="gridRadios1" value="1" >
                                    @endif
                                        {{trans('products.active')}}
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    @if($item->has_type == 0)
                                    <input class="form-check-input" type="radio" name="has_type" id="gridRadios2" value="0" checked>
                                    @else
                                        <input class="form-check-input" type="radio" name="has_type" id="gridRadios2" value="0">
                                    @endif
                                    {{trans('products.closed')}}

                                </label>
                            </div>
                        </div>
                    </div>
                </fieldset>


                <div class="form-group mb-5 custom-file">
                    <label for="image" class="custom-file-label">{{trans('SubCategory.image')}}</label>
                    <input type="file" class="form-control-file custom-file-input" id="image" name="file" placeholder="Upload Image" >
                </div>

                <button type="submit" class="btn btn-primary">{{trans('SubCategory.submit')}}</button>
            </form>
        </div>
    </div>
    <script>

    </script>
@endsection
