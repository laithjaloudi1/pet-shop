@extends('layout.app')
@section('content')

<div class="container">

    <div class="row">
        <div class="col-md-9">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">{{trans('attributes.name_en')}}</th>
                    <th scope="col">{{trans('attributes.name_ar')}}</th>
                    <th scope="col">{{trans('attributes.price')}}</th>
                    <th scope="col">{{trans('attributes.value')}}</th>
                </tr>
                </thead>
                    @if(count($attrbuitres))
                        @foreach($attrbuitres as $att)
                            @foreach($att->values as $values)
                                <tr>
                                    <th scope="row">{{$values->id}}</th>
                                    <td>{{$att->name_en}}</td>
                                    <td>{{$att->name_ar}}</td>
                                    <td>{{$values->price}}</td>
                                    <td>{{$values->value}}</td>
                                </tr>
                            @endforeach
                        @endforeach
                    @endif



            </table>
        </div>
        <div class="col-md-3">
                <div class="bgc-white" style="padding-top: 16px;padding-left: 16px;">
                    <h6 class="c-grey-900">{{trans('attribute.create')}}</h6>
                    <form action="{{route('attributes.store',$id)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="username">{{trans('products.att_name')}}</label>
                            <input type="text" class="form-control" id="username"  name="name_en" placeholder="Enter description" >
                        </div>
                        <div class="form-group">
                            <label for="username">{{trans('products.att_name')}}</label>
                            <input type="text" class="form-control" id="username"  name="name_ar" placeholder="Enter description" >
                        </div>
                        <div class="form-group att">
                            <label for="username">{{trans('products.att_value')}}</label>
                            <input type="text" class="form-control" id="username"  name="attributes[0][value]" placeholder="Enter description" >
                        </div>
                        <div class="form-group att">
                            <label for="username">{{trans('products.att_price')}}</label>
                            <input type="number"  step="0.05" class="form-control" id="username"  name="attributes[0][price]" placeholder="Enter description" >
                        </div>
                        <div id="append">

                        </div>
                        <a class="btn btn-success" onclick="add()">Add</a>
                        <button type="submit" class="btn btn-primary">{{trans('attribute.submit')}}</button>
                    </form>
                </div>
        </div>
    </div>
</div>



    <script>
        function add() {
            let count =$('.att').length;
            let index = count/2;
            $('#append').append(`<div class="form-group att">
                    <label for="username"></label>
                    <input type="text" class="form-control" id="username"  name="attributes[${index}][value]" placeholder="Enter description" >
                </div>
                <div class="form-group att">
                    <label for="username"></label>
                    <input type="number" class="form-control" id="username"  name="attributes[${index}][price]" placeholder="Enter description" >
                </div>`)
        }
    </script>


@endsection
