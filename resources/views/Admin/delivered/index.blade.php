@extends('layout.app')
@section('content')

    <div id='mainContent'>
        <div class="container-fluid">
            <h4 class="c-grey-900 mT-10 mB-30">{{trans('Is_delivered.list')}}</h4>
            <div class="row">
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20 mB-20">
                        <h4 class="c-grey-900 mB-20">{{trans('Is_delivered.list')}}

                        </h4>

                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">{{trans('order.name')}}</th>
                                <th scope="col">{{trans('order.phone_number')}}</th>
                                <th scope="col">{{trans('order.email')}}</th>
                                <th scope="col">{{trans('order.total_price')}}</th>
                                <th scope="col">{{trans('order.country')}}</th>
                                <th scope="col">{{trans('order.city')}}</th>
                                <th scope="col">action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($data as $item)
                                <tr>
                                    <th scope="row">{{$item->id}}</th>
                                    <td>{{$item->first_name}} {{$item->last_name}}</td>
                                    <td>{{$item->phone_number}}</td>
                                    <td>{{$item->email}}</td>
                                    <td>{{$item->total_price}}</td>
                                    <td>{{$item->country}}</td>
                                    <td>{{$item->city}}</td>
                                    <td>
                                        <a href="{{route('orders.show',$item->id)}}" class="btn btn-primary btn-block btn-sm">{{trans('order.details')}}</a>
                                        <br>
                                        <form method="post" action="{{route('delivered',$item->id)}}">
                                            <input type="hidden" value="patch" name="_method">
                                            @csrf
                                            <button type="submit" class="btn btn-success btn-block btn-sm">{{trans('order.delivered')}}</button>
                                        </form>
                                        <br>
                                        <form method="post" action="{{route('cancel',$item->id)}}">
                                            <input type="hidden" value="patch" name="_method">
                                            @csrf
                                            <button type="submit" class="btn btn-danger btn-block btn-sm">{{trans('order.cancel')}}</button>
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                <tr><td class="text-center" colspan="8">empty</td></tr>
                            @endforelse
                            </tbody>
                        </table>
                                                {{$data->appends(Request::all())->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
