<?php
/**
 * Created by PhpStorm.
 * User: laith
 * Date: 03/04/20
 * Time: 04:28 م
 */


return [
    'categories'=>'sub categories',
    'list'=>'Sub Categories List',
    'title_en'=>'Title in english',
    'title_ar'=>'Title in arabic',
    'description_en'=>'description in english',
    'description_ar'=>'description in arabic',
    'image'=>'image',
    'action'=>'Actions',
    'create'=>'Create Sub Category',
    'show'=>'Show Sub Category',
    'edit'=>'Edit Sub Category',
    'delete'=>'Delete Sub Category',
    'created_by'=>'Admin name',
    'created_at'=>'create date',
    'update_at'=>'last update date',
    'back'=>'back',
    'info'=>'Sub Category Info',

];
