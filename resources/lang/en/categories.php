<?php
/**
 * Created by PhpStorm.
 * User: laith
 * Date: 03/04/20
 * Time: 01:40 م
 */

return [
  'categories'=>'categories',
  'list'=>'Categories List',
  'title_en'=>'Title in english',
  'title_ar'=>'Title in arabic',
  'description_en'=>'description in english',
  'description_ar'=>'description in arabic',
  'image'=>'image',
  'action'=>'Actions',
  'create'=>'Create Category',
  'show'=>'Show Category',
  'edit'=>'Edit Category',
  'delete'=>'Delete Category',
  'created_by'=>'Admin name',
  'created_at'=>'create date',
  'update_at'=>'last update date',
  'back'=>'back',
  'info'=>'Category Info',

];
