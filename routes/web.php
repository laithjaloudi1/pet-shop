<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});




Auth::routes();

Route::get('/', 'Website\HomeController@index')->name('index');
Route::get('about', 'Website\AboutController@index')->name('about-us');
Route::get('contact', 'Website\ContactController@index')->name('contact-us');
Route::get('categoryshow', 'Website\CategoryController@show')->name('categoryshow');
Route::get('category/{id}','Website\CategoryController@index')->name('website_category');
Route::get('subCategory/{id}','Website\SubCategoryController@index')->name('website_subcategory');
Route::get('products/{id}','Website\ProductsController@index')->name('website_products');
Route::get('product/{id}','Website\ProductsController@show')->name('website_product');
Route::Post('guest-login','Website\HomeController@gust')->name('guest_login');

Route::get('/search','Website\ProductsController@search');
Route::group(['middleware'=>['auth']], function () {
    Route::get('checkout/{cart_id}','Website\CartController@checkoutForm')->name('website_cart_checkout');
    Route::post('checkout/{cart_id}','Website\CartController@checkout')->name('website_cart_checkout');
    Route::post('cart/{product_id}','Website\CartController@store')->name('website_cart_store');
    Route::delete('cart/{product_id}','Website\CartController@destroy')->name('website_cart_destroy');
    Route::get('cart','Website\CartController@index')->name('website_cart_view');
    Route::post('change-quantity/{id}','Website\CartController@changeQuantity');
});



Route::group(['prefix'=>'admin','middleware'=>['auth','is_admin']],function (){
    Route::resource('categories','Admin\CategoriesController');
    Route::resource('sub-categories','Admin\SubCategoryController');
    Route::resource('has-types','Admin\HasTypeController');
    Route::resource('brands','Admin\BrandController');
    Route::resource('products','Admin\ProductsController');
    Route::resource('products/{id}/attributes','Admin\ProductsAttributesController');
    Route::resource('orders','Admin\OrdersController');
    Route::resource('admin','Admin\AdminController');
    Route::resource('users','Admin\UsersController');
    Route::resource('delivered','Admin\DeliveredController');
    Route::resource('cancel','Admin\CancelController');
    Route::patch('orders-delivered/{id}','Admin\OrdersController@delivered')->name('delivered');
    Route::patch('orders-cancel/{id}','Admin\OrdersController@cancel')->name('cancel');
});
