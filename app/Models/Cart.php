<?php


namespace App\Models;


use App\User;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $guarded=[];

    protected $table='carts';

    protected $appends=['totalPrice'];

    public function user()
    {
        return $this->hasOne(User::class,'user_id','id');
    }

    public function CartProduct()
    {
        return $this->hasMany(CartProduct::class,'cart_id','id');
    }

    public function getTotalPriceAttribute()
    {
        $total=0.0;

        foreach ($this->CartProduct as $item)
        {
            $total=($item->product->Price($item->id) * $item->quantity)+$total;
        }

        return $total;
    }
}
