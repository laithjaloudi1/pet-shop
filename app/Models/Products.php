<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    public function type()
    {
        return $this->morphTo();
    }

    protected $guarded=[];

    public function attributes()
    {
        return $this->hasMany(Attributes::class,'product_id','id');
    }

    public function Price($id)
    {
        $cartProduct=CartProduct::where('id',$id)->first();
        if ($cartProduct->product->attributes()->count() > 0){
            $price=0.0;
            $attributes=json_decode($cartProduct->attribute_value_id,true);
            foreach ($attributes as $attribute)
            {
                $price=AttributesValues::where('id',$attributes)->first()->price+$price;
            }
            return $price;
        }else{
            return $cartProduct->product->price;
        }
    }
}
