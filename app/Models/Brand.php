<?php


namespace App\Models;


use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brand extends Model
{

    use SoftDeletes;

    protected $fillable=[
        'title_en',
        'title_ar',
        'description_en',
        'description_ar',
        'image',
        'created_by',
    ];


    public function admin()
    {
        return $this->hasOne(User::class,'id','created_by');
    }

}
