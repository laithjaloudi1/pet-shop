<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class CartProduct extends Model
{
    protected $table='carts_products';

    protected $guarded=[];

    public function cart()
    {
        return $this->hasOne(Cart::class,'id','cart_id');
    }

    public function product()
    {
        return $this->hasOne(Products::class,'id','product_id');
    }
}
