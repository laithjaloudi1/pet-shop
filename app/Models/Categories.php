<?php
/**
 * Created by PhpStorm.
 * User: laith
 * Date: 03/04/20
 * Time: 01:38 م
 */

namespace App\Models;


use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Categories extends Model
{
    use SoftDeletes;

    protected $fillable=[
      'title_en',
      'title_ar',
      'description_en',
      'description_ar',
      'image',
      'created_by',
    ];


    public function admin()
    {
        return $this->hasOne(User::class,'id','created_by');
    }

    public function SubCategory()
    {
        return $this->hasMany(SubCategory::class,'category_id','id');
    }
}
