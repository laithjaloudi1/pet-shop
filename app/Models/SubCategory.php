<?php
/**
 * Created by PhpStorm.
 * User: laith
 * Date: 03/04/20
 * Time: 02:05 م
 */

namespace App\Models;


use App\User;
use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $table='sub_category';

    protected $fillable=[
        'title_en',
        'title_ar',
        'description_en',
        'description_ar',
        'image',
        'created_by',
        'category_id',
        'has_type'
    ];

    public function admin()
    {
        return $this->hasOne(User::class,'id','created_by');
    }

    public function category()
    {
        return $this->hasOne(Categories::class,'id','category_id');
    }

    public function hastype()
    {
        return $this->hasMany(SubCategory::class,'sub_category_id','id');
    }
}
