<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Attributes extends Model
{
    protected $guarded=[];

    public function values()
    {
        return $this->hasMany(AttributesValues::class,'attribute_id','id');
    }
}
