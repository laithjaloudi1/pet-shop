<?php


namespace App\Models;


use App\User;
use Illuminate\Database\Eloquent\Model;

class HasType extends model
{
     protected $table = 'sub_category_types';
     protected $fillable=[
        'title_en',
        'title_ar',
        'description_en',
        'description_ar',
        'image',
        'created_by',
        'sub_category_id'
    ];

    public function admin()
    {
        return $this->hasOne(User::class,'id','created_by');
    }

    public function subcategory()
    {
        return $this->hasOne(SubCategory::class,'id','sub_category_id');
    }
}

