<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class AdminController extends Controller
{
    public function index(){
        $user = User::where('is_admin',1)->paginate();
        return view('Admin.admins.index')->with(['data' => $user]);
    }

    public function create()
    {
        return view('Admin.admins.forms.create');
    }

   public function store(Request $request)
{

    $request->validate([
        'name' => ['required', 'string', 'max:255'],
        'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        'password' => ['required', 'string', 'min:8'],
    ]);


    $request->offsetSet('created_by', Auth::id());
    $request->offsetSet('is_admin', 1);
    User::create(['name'=>$request['name'],
        'email'=>$request['email'],
        'password'=>Hash::make($request['password']),
            'is_admin'=>$request['is_admin']
]);

    return redirect()->route('admin.index');
}
    public function destroy($id)
    {
        User::query()->where('id', $id )->delete();
        return redirect()->route('admin.index');
    }

}
