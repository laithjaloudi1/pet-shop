<?php


namespace App\Http\Controllers\Admin;


use App\Models\Cart;

class CancelController
{
    public function index(){
        $cancel=Cart::where('is_paid',1)->where('is_delivered',2)->paginate();
        return view('Admin.cancel.index')->with(['data'=>$cancel]);
    }
}
