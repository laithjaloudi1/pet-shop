<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Cart;

class DeliveredController extends Controller
{
    public function index(){
        $is_delivered=Cart::where('is_paid',1)->where('is_delivered',1)->paginate();
        return view('Admin.delivered.index')->with(['data'=>$is_delivered]);
    }
}
