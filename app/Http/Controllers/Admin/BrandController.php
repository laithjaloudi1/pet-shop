<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class BrandController extends Controller
{
    public function index()
    {
        $brands = Brand::query()->paginate();
        return view('Admin.brands.index')->with(['data' => $brands]);
    }

    public function create()
    {
        return view('Admin.brands.forms.create');
    }

    public function store(Request $request)
    {

        $request->validate([
            'title_en' => 'required',
            'title_ar' => 'required',
            'description_en' => 'required',
            'description_ar' => 'required',
            'file' => 'required'
        ]);

        $request->offsetSet('created_by', Auth::id());
        $image = $request->file('file');
        $extension = $image->getClientOriginalExtension();
        Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));
        $request->offsetSet('image', $image->getFilename() . '.' . $extension);

        Brand::create($request->all());

        return redirect()->route('brands.index');
    }

    public function show($id)
    {
        $item = Brand::find($id);
        return view('Admin.brands.show')->with(['item' => $item]);
    }

    public function edit($id)
    {
        $item = Brand::find($id);
        return view('Admin.brands.forms.edit')->with(['item' => $item]);
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'title_en' => 'required',
            'title_ar' => 'required',
            'description_en' => 'required',
            'description_ar' => 'required',
        ]);
        $attributes = [
            'title_en' => $request->title_en,
            'title_ar' => $request->title_ar,
            'description_en' => $request->description_en,
            'description_ar' => $request->description_ar,
        ];

        if (isset($request->file)) {
            $image = $request->file('file');
            $extension = $image->getClientOriginalExtension();
            Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));
            $request->offsetSet('image', $image->getFilename() . '.' . $extension);
            $attributes['image'] = $image->getFilename() . '.' . $extension;
        }

        Brand::query()->where('id', $id)->update($attributes);

        return redirect()->route('brands.index');
    }

    public function destroy($id)
    {
        Categories::query()->where('id', $id)->delete();
        return redirect()->route('brands.index');
    }
}
