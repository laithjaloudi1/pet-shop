<?php


namespace App\Http\Controllers\Admin;


use App\Models\Attributes;
use App\Models\AttributesValues;
use Illuminate\Http\Request;

class ProductsAttributesController
{
    public function create($id)
    {
        $attrbuitres=Attributes::where('product_id',$id)->get();
        return view("Admin.attributes.create")->with(['id'=>$id,'attrbuitres'=>$attrbuitres]);
    }

    public function store($id,Request $request)
    {

        $attributes=Attributes::create([
            'name_en'=>$request->name_en,
            'name_ar'=>$request->name_ar,
            'product_id'=>$id
        ]);
        foreach ( $request->get('attributes') as $value)
        {
                AttributesValues::create([
                    'attribute_id'=>$attributes->id,
                    'value'=>$value['value'],
                    'price'=>$value['price']
                ]);

        }
        return redirect()->back();
    }
}
