<?php


namespace App\Http\Controllers\Admin;
use App\Models\Categories;
use App\Models\HasType;
use App\Models\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class HasTypeController
{
///$subcategories=SubCategory::where('has_type',1)->get();

    public function index()
    {
        $subcategories=HasType::paginate();
        return view('Admin.has-type.index')->with(['data' => $subcategories]);
    }

    public function create()
    {
        $subCategories=SubCategory::where('has_type',1)->get();

        return view('Admin.has-type.forms.create')->with(['subCategories'=>$subCategories]);
    }

    public function store(Request $request)
    {

        $request->validate([
            'title_en' => 'required',
            'title_ar' => 'required',
            'description_en' => 'required',
            'description_ar' => 'required',
            'file' => 'required',
            'sub_category_id'=>'required|exists:sub_category,id'
        ]);

        $request->offsetSet('created_by', Auth::id());
        $image = $request->file('file');
        $extension = $image->getClientOriginalExtension();
        Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));
        $request->offsetSet('image', $image->getFilename() . '.' . $extension);

        HasType::create($request->all());

        return redirect()->route('has-types.index');
    }

    public function show($id)
    {
        $item = HasType::find($id);
        return view('Admin.has-type.show')->with(['item' => $item]);
    }

    public function edit($id)
    {
        $item = HasType::find($id);
        $subcategories=SubCategory::all();
        return view('Admin.has-type.forms.edit')->with(['item' => $item,'subcategories'=>$subcategories]);
    }

    public function update($id, Request $request)
    {


        $request->validate([
            'title_en' => 'required',
            'title_ar' => 'required',
            'description_en' => 'required',
            'description_ar' => 'required',
            'sub_category_id'=>'required|exists:sub_category,id'
        ]);
        $attributes = [
            'title_en' => $request->title_en,
            'title_ar' => $request->title_ar,
            'description_en' => $request->description_en,
            'description_ar' => $request->description_ar,
            'sub_category_id'=> $request->sub_category_id,

            //'category_type'=> SubCategory::class,
            //'category_id'=> $request->sub_category_id,

            //else 'category_type'=> HasType::class,
        ];

        if (isset($request->file)) {
            $image = $request->file('file');
            $extension = $image->getClientOriginalExtension();
            Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));
            $request->offsetSet('image', $image->getFilename() . '.' . $extension);
            $attributes['image'] = $image->getFilename() . '.' . $extension;
        }
        HasType::query()->where('id', $id)->update($attributes);

        return redirect()->route('has-types.index');
    }

    public function destroy($id)
    {
        HasType::query()->where('id', $id)->delete();
        return redirect()->route('has-types.index');
    }

}
