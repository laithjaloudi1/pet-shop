<?php


namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\attributes;
use App\Models\AttributesValues;
use App\Models\Brand;
use App\Models\HasType;
use App\Models\Products;
use App\Models\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ProductsController extends Controller
{
    public function index()
    {
        $data=Products::query()->paginate();

        return view('Admin.products.index')->with(['data'=>$data]);
    }

    public function create()
    {
        $brands=Brand::all();
        $subCategories=SubCategory::where('has_type',0)->get();
        $subCategoriesTypes=HasType::all();
        return view('Admin.products.forms.create')->with([
            'brands'=>$brands,
            'subCategories'=>$subCategories,
            'subCategoriesTypes'=>$subCategoriesTypes
        ]);
    }

    public function store(Request $request)
    {


        $request->validate([
            'title_en' => 'required',
            'title_ar' => 'required',
            'description_en' => 'required',
            'description_ar' => 'required',
            'brand_id'=>'required|exists:brands,id',
            'price'=>'required',
            'has_type'=>'required',
            'sub_category'=>'required',
            'sub_category_types'=>'required',
            'file' => 'required'
        ]);
        $image = $request->file('file');
        $extension = $image->getClientOriginalExtension();
        Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));

        $attributes=[
          'title_en'=>$request->title_en,
          'title_ar'=>$request->title_ar,
          'description_en'=>$request->description_en,
          'description_ar'=>$request->description_ar,
            'price'=>$request->price,
          'brand_id'=>$request->brand_id,
          'created_by'=>Auth::id(),
          'image'=>$image->getFilename() . '.' . $extension,

        ];

        if ($request->has_type)
        {
            $attributes['category_type']=HasType::class;
            $attributes['category_id']=$request->sub_category_types;
        }else{
            $attributes['category_type']=SubCategory::class;
            $attributes['category_id']=$request->sub_category;
        }

       $product=Products::create($attributes);

        return redirect()->route('products.index');

    }

    public function destroy($id)
    {
        $product = Products::query()->where('id', $id)->delete();
        return redirect()->route('products.index');
    }
}
