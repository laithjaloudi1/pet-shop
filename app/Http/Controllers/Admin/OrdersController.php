<?php
/**
 * Created by PhpStorm.
 * User: laith
 * Date: 26/04/20
 * Time: 12:24 ص
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Cart;

class OrdersController extends Controller
{

    public function index()
    {
        // history  $orders=Cart::where('is_paid',1)->where('is_delivered','!=',0)
        //            ->paginate();

        $orders=Cart::where('is_paid',1)->where('is_delivered',0)
            ->paginate();
        return view('Admin.order.index')->with(['data'=>$orders]);
    }

    public function show($id)
    {
        $order=Cart::find($id);

        return view('Admin.order.show')->with(['item'=>$order]);
    }

    public function delivered($id)
    {
        $order=Cart::where('id',$id)->update([
            'is_delivered'=>1
        ]);

        return redirect()->back();
    }

    public function cancel($id)
    {
        $order=Cart::where('id',$id)->update([
            'is_delivered'=>2
        ]);

        return redirect()->back();
    }
}
