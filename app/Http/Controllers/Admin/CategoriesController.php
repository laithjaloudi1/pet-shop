<?php
/**
 * Created by PhpStorm.
 * User: laith
 * Date: 03/04/20
 * Time: 01:27 م
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Categories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class CategoriesController extends Controller
{
    public function index()
    {
        $categories = Categories::query()->paginate();
        return view('Admin.categories.index')->with(['data' => $categories]);
    }

    public function create()
    {
        return view('Admin.categories.forms.create');
    }

    public function store(Request $request)
    {

        $request->validate([
            'title_en' => 'required',
            'title_ar' => 'required',
            'description_en' => 'required',
            'description_ar' => 'required',
            'file' => 'required'
        ]);

        $request->offsetSet('created_by', Auth::id());
        $image = $request->file('file');
        $extension = $image->getClientOriginalExtension();
        Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));
        $request->offsetSet('image', $image->getFilename() . '.' . $extension);

        Categories::create($request->all());

        return redirect()->route('categories.index');
    }

    public function show($id)
    {
        $item = Categories::find($id);
        return view('Admin.categories.show')->with(['item' => $item]);
    }

    public function edit($id)
    {
        $item = Categories::find($id);
        return view('Admin.categories.forms.edit')->with(['item' => $item]);
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'title_en' => 'required',
            'title_ar' => 'required',
            'description_en' => 'required',
            'description_ar' => 'required',
        ]);
        $attributes = [
            'title_en' => $request->title_en,
            'title_ar' => $request->title_ar,
            'description_en' => $request->description_en,
            'description_ar' => $request->description_ar,
        ];

        if (isset($request->file)) {
            $image = $request->file('file');
            $extension = $image->getClientOriginalExtension();
            Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));
            $request->offsetSet('image', $image->getFilename() . '.' . $extension);
            $attributes['image'] = $image->getFilename() . '.' . $extension;
        }

        Categories::query()->where('id', $id)->update($attributes);

        return redirect()->route('categories.index');
    }

    public function destroy($id)
    {
        Categories::query()->where('id', $id)->delete();
        return redirect()->route('categories.index');
    }
}
