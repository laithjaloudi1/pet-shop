<?php
/**
 * Created by PhpStorm.
 * User: laith
 * Date: 03/04/20
 * Time: 04:24 م
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\Categories;
use App\Models\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
class SubCategoryController extends Controller
{

    public function index()
    {
        $subCategories = SubCategory::query()->paginate();
        return view('Admin.sub-category.index')->with(['data' => $subCategories]);
    }

    public function create()
    {
        $categories=Categories::all();

        return view('Admin.sub-category.forms.create')->with(['categories'=>$categories]);
    }

    public function store(Request $request)
    {

        $request->validate([
            'title_en' => 'required',
            'title_ar' => 'required',
            'description_en' => 'required',
            'description_ar' => 'required',
            'file' => 'required',
            'category_id'=>'required|exists:categories,id'
        ]);

        $request->offsetSet('created_by', Auth::id());
        $image = $request->file('file');
        $extension = $image->getClientOriginalExtension();
        Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));
        $request->offsetSet('image', $image->getFilename() . '.' . $extension);

        SubCategory::create($request->all());

        return redirect()->route('sub-categories.index');
    }

    public function show($id)
    {
        $item = SubCategory::find($id);
        return view('Admin.sub-category.show')->with(['item' => $item]);
    }

    public function edit($id)
    {
        $item = SubCategory::find($id);
        $categories=Categories::all();
        return view('Admin.sub-category.forms.edit')->with(['item' => $item,'categories'=>$categories]);
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'title_en' => 'required',
            'title_ar' => 'required',
            'description_en' => 'required',
            'description_ar' => 'required',
            'category_id'=>'required|exists:categories,id'
        ]);
        $attributes = [
            'title_en' => $request->title_en,
            'title_ar' => $request->title_ar,
            'description_en' => $request->description_en,
            'description_ar' => $request->description_ar,
            'category_id'=> $request->category_id,
            'has_type'=> $request->has_type
        ];

        if (isset($request->file)) {
            $image = $request->file('file');
            $extension = $image->getClientOriginalExtension();
            Storage::disk('public')->put($image->getFilename() . '.' . $extension, File::get($image));
            $request->offsetSet('image', $image->getFilename() . '.' . $extension);
            $attributes['image'] = $image->getFilename() . '.' . $extension;
        }

        SubCategory::query()->where('id', $id)->update($attributes);

        return redirect()->route('sub-categories.index');
    }

    public function destroy($id)
    {
        SubCategory::query()->where('id', $id)->delete();
        return redirect()->route('sub-categories.index');
    }

}
