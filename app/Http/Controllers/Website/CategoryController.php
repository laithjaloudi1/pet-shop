<?php


namespace App\Http\Controllers\Website;


use App\Http\Controllers\Controller;
use App\Models\Categories;
use App\Models\SubCategory;

class CategoryController extends Controller
{
    public function index($id)
    {
        $categories=Categories::all();
        $category=Categories::find($id);
        $subCategories=SubCategory::where('category_id',$id)->get();
        return view('Website.category')->with(['categories'=>$categories,'category'=>$category,'subCategories'=>$subCategories]);
    }

    public function show(){
        $categories=Categories::all();
        return view('Website.categoryshow')->with(['categories'=>$categories]);

    }

}
