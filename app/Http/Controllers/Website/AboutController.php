<?php


namespace App\Http\Controllers\Website;


use App\Http\Controllers\Controller;
use App\Models\Categories;

class AboutController extends Controller
{
    public function index()
    {
        $categories=Categories::all();
        return view('Website.about')->with(['categories'=>$categories]);
    }
}
