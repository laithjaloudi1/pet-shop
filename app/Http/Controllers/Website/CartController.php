<?php


namespace App\Http\Controllers\Website;


use App\Http\Controllers\Controller;
use App\Models\AttributesValues;
use App\Models\Cart;
use App\Models\CartProduct;
use App\Models\Categories;
use App\Models\Products;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class CartController extends Controller
{

    public function index()
    {
        $categories=Categories::all();
        $user=Auth::user();
        $cart=$user->hasCart;
        $data=CartProduct::query()->paginate();
        return view('Website.cart')->with(['user'=>$user,'cart'=>$cart,'categories'=>$categories,'data'=>$data]);
    }

    public function store($productId,Request $request)
    {
        $user=Auth::user();

        if ($user->hasCart == null)
        {
            $cart=Cart::create([
               'user_id'=>Auth::id(),
               'is_paid'=>0,
                'total_price'=>0
            ]);
        }else{
            $cart=$user->hasCart;
        }
        $product=Products::find($productId);
        $price=0;
        if ($product->attributes->count()>0)
        {
            $attValues=json_encode($request->values);
            foreach ($request->values as $item)
            {
                $values=AttributesValues::where('id',$item)->first();
                $price=$price+$values->price;
            }
        }else{
            $attValues=json_encode([]);
            $price=$product->price;
        }

        $ProductExists=CartProduct::where('cart_id',$cart->id)
            ->where('product_id',$productId)
            ->where('attribute_value_id',$attValues)
            ->first();

        if (!$ProductExists)
        {
            CartProduct::create([
                'product_id'=>$productId,
                'cart_id'=>$cart->id,
                'price'=>$price,
                'quantity'=>1,
                'attribute_value_id'=>$attValues,

            ]);

        }else{
            $ProductExists->quantity=$ProductExists->quantity+1;
            $ProductExists->price=$ProductExists->price+$price;
            $ProductExists->save();
        }

        return redirect()->back();
    }

    public function checkoutForm($id)
    {
        $categories=Categories::all();
        $user=Auth::user();
        $cart=$user->hasCart;
        return view('Website.checkout')->with(['categories'=>$categories,'cart'=>$cart]);
    }


    public function checkout($id,Request $request)
    {
        $cart=Cart::find($id);
        Cart::where('id',$id)->update([
            'is_paid'=>1,
            'total_price'=>$cart->totalPrice,
            'first_name'=>$request->first_name,
            'last_name'=>$request->last_name,
            'email'=>$request->email,
            'country'=>$request->country,
            'city'=>$request->city,
            'street'=>$request->street,
            'block'=>$request->block,
            'apartment_number'=>$request->apartment_number,
            'phone_number'=>$request->phone_number,
        ]);

        return redirect()->route('index');

    }

    public function changeQuantity($id,Request $request)
    {

       $cart= CartProduct::where('id',$id)->first();
        $cart->quantity=$request->quantity;
        $cart->save();


        return response()->json(['status'=>true,'total'=>$cart->cart->totalPrice]);
    }

    public function destroy($id)
    {
        $cart=CartProduct::find($id);
        CartProduct::query()->where('id',$id)->delete();
        return redirect()->route('website_cart_view');
    }

}
