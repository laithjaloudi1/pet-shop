<?php


namespace App\Http\Controllers\Website;


use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Categories;
use App\Models\HasType;
use App\Models\Products;
use App\Models\SubCategory;
use Illuminate\Http\Request;
use test\Mockery\Fixtures\MethodWithVoidReturnType;

class ProductsController extends Controller
{

    // Id may be has type id or sub category id

    public function index($id,Request $request)
    {

        $categories=Categories::all();
        $brands=Brand::all();

        $type = $request->type;
        if ($type != null )
        {
         if ($type == 0 || $type == 1 )
         {
             $model='';
             if ($type == 0 )
             {
                 $model=SubCategory::class;
             }else{
                 $model=HasType::class;
             }

             $products = Products::query()->where('category_id',$id)
                 ->where('category_type',$model);


             if (isset($request->search) && $request->search != null)
             {
                 $products=$products->where('title_en', 'like', "%" . $request->search . "%")
                 ->orWhere('title_ar', 'like', "%" . $request->search . "%");
             }

             if (isset($request->brand) && $request->brand != null)
             {
                 $products=$products->where('brand_id',$request->brand);
             }

             if (isset($request->order) && $request->order != null)
             {
                 if ($request->order == 'order')
                 {
                     $products=$products->orderBy('id');
                 }else{
                     $products=$products->orderBy('created_at');

                 }
             }

             $products= $products->paginate();

             return view('Website.products')->with(['products'=>$products,'categories'=>$categories,'brands'=>$brands,'type'=>$type,'category_id'=>$id]);
         }  else{
             return redirect()->back();

         }
        }else{
            return redirect()->back();
        }
    }

    public function show($id)
    {
        $categories=Categories::all();
        $product = Products::query()->where('id',$id)->get();
        return view('Website.product')->with(['product'=>$product,'categories'=>$categories]);
    }

    public function search(Request $request){
        $categories=Categories::all();
        $search = $request->get('search');
        $product = Products::query()->where('title_en','like','%'.$search.'%')->paginate(5);
        return view('Website.productsearch')->with(['categories'=>$categories,'product'=>$product]);

    }

}
