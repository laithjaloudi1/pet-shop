<?php


namespace App\Http\Controllers\Website;


use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Categories;
use App\Models\Products;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{

    public function index()
    {
        $brands = Brand::all();
        $categories=Categories::all();
        $product=Products::orderBy('created_at', 'desc')->paginate(4);
        return view('Website.index')->with(['categories'=>$categories,'brands'=>$brands,'product'=>$product]);
    }

    public function gust()
    {

        $random='guest'.rand(1, 1000000);
        $randPass=rand(1,100000000);

       $user= User::create([
            'name' => 'guest',
            'email' => $random.'@pet.com',
            'password' => Hash::make($randPass),
        ]);

        Auth::login($user);

        return redirect()->route('index');

    }
}
