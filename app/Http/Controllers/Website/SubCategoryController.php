<?php


namespace App\Http\Controllers\Website;


use App\Models\Categories;
use App\Models\HasType;
use App\Models\SubCategory;

class SubCategoryController
{
    public function index($id)
    {
        $categories=Categories::all();
        $subCategories=SubCategory::all();
        $subCategory=SubCategory::find($id);
        $hastypes=HasType::where('sub_category_id',$id)->get();
        return view('Website.subcategory')->with(['subCategories'=>$subCategories,'subCategory'=>$subCategory,'hastypes'=>$hastypes,'categories'=>$categories]);
    }
}
