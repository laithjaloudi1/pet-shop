//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches
var page = 1
$(".next").click(function(){

    page = page +1
    if(animating) return false;
    animating = true;

    current_fs = $(this).parent();
    next_fs = $(this).parent().next();


    //activate next step on progressbar using the index of next_fs
    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

    //show the next fieldset
    next_fs.show();
    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
        step: function(now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale current_fs down to 80%
            scale = 1 - (1 - now) * +1.2;
            //2. bring next_fs from the right(50%)
            left = (now * 20)+"%";
            //3. increase opacity of next_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({
                'transform': 'scale('+scale+')',
                'position': 'absolute'
            });
            next_fs.css({'left': left, 'opacity': opacity});
        },
        duration: 800,
        complete: function(){
            current_fs.hide();
            animating = false;
        },
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });

});

$(".previous").click(function(){
    page = page - 1
    if(animating) return false;
    animating = true;

    current_fs = $(this).parent();
    previous_fs = $(this).parent().prev();

    //de-activate current step on progressbar
    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

    //show the previous fieldset
    previous_fs.show();
    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
        step: function(now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale previous_fs from 80% to 100%
            scale = 0.8 + (1 - now) * 0.2;
            //2. take current_fs to the right(50%) - from 0%
            left = ((1-now) * 50)+"%";
            //3. increase opacity of previous_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({'left': left});
            previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
        },
        duration: 800,
        complete: function(){
            current_fs.hide();
            animating = false;
        },
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
});

$(document).ready(function () {
   $('.addresslist').first().addClass('address-checked');
});

$(".saveadress").on('click',function () {
    var addressTitle = $('.addresssearch').val();
    var address = $('.address').val();
    var city = $('.city').val();
    var country = $('.country').val();
    var landmark = $('.landmark').val();
    var pincode = $('.pincode').val();
    var lat = $('#startLat').text();
    var lng =$('#startLon').text();
    if (addressTitle) {
        if (address.length>1 && city.length>1 && country.length>1 && landmark.length>1 && pincode.length>=1) {
            console.log(address);
            $.ajax({

                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'post',
                url: '/address',
                data: {
                    title: addressTitle,
                    address: address,
                    city: city,
                    country: country,
                    landmark: landmark,
                    pin_code: pincode,

                },
                success: function (data) {
                    alert('address saved successfully');
                    $('.addresssearch').val('');
                    $('.address').val('');
                    $('.city').val('');
                    $('.country').val('');
                    $('.landmark').val('');
                    $('.pincode').val('');
                    $('#startLon').val('');
                    $('#startLat').val();

                    var loction =
                        '<div class="addresslist" id="' + data.id + '">\n' +
                        '<p class="adresstitle" ><i class="fa fa-map-marker" aria-hidden="true"></i> ' + data.title + ' <span class="float-right"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></p>\n' +
                        '</div>'

                    $('.selectaddress').after(loction);
                }
            })

        }else if(lat.length>1 && lng.length>1)
        {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'post',
                url: '/address',
                data: {
                    title: addressTitle,
                    lat:lat,
                    lng:lng

                },
                success: function (data) {
                    alert('address saved successfully');
                    var loction =
                        '<div class="addresslist" id="' + data.id + '">\n' +
                        '<p class="adresstitle" ><i class="fa fa-map-marker" aria-hidden="true"></i> ' + data.title + ' <span class="float-right"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></p>\n' +
                        '</div>'
                    $('.selectaddress').after(loction);
                }
            })
        }
        else
        {
            alert('please fill all fields or use current location ')
        }

    }
    else{
        alert('please enter title for the address');
    }
});

    function updateAddress(id)
    {

        if (id)
        {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'get',
                url: '/address/'+id,

                success: function (data) {
                    $('#address-id').val(data.id)
                    $('#address-title').val(data.title)
                    $('#address').val(data.address)
                    $('#city').val(data.city)
                    $('#Country').val(data.country)
                    $('#landmark').val(data.landmark)
                    $('#pin-code').val(data.pin_code)
                }
            })
        }
    }

    $('#update-address').on('click',function () {
        var title = $('#address-title').val();
        var address = $('#address').val();
        var city = $('#city').val();
        var country = $('#Country').val();
        var landmark = $('#landmark').val();
        var pincode = $('#pin-code').val();
        var id =$('#address-id').val();


        if (title)
        {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    'Accept':'application/json'
                },
                type: 'post',
                url: '/address/'+id,
                data:{
                  title:title,
                  address:address,
                  city:city,
                  country:country,
                  landmark:landmark,
                  pin_code:pincode

                },

                success: function (data) {
                    alert('update successfully')
                    $('#title'+id).text()
                    $('#title'+id).text(data.title)
                }
            })
        }
    })


    $(document).ready(function () {
        $('#address-container').on('click','.addresslist',function () {
            var addresslist=$('.addresslist');
            addresslist.css('background-color','#fff');
            $(this).css('background-color','#FBC000');

            if (addresslist.hasClass('address-checked'))
            {
                addresslist.removeClass('address-checked')
            }
            $(this).addClass('address-checked');

        });
    });

    $(document).ready(function () {
        $('.pickupoption').on('click','.iconbg',function () {
            var iconbag=$('.iconbg');
            iconbag.css('background-color','#878787');
            $(this).css('background-color','#ea535f');
            if (iconbag.hasClass('delivery-checked'))
            {
                iconbag.removeClass('delivery-checked')
            }
            $(this).addClass('delivery-checked');
        });
    });


    $('.paymentoptions').on('click','.pay',function () {

        var pay =$('.pay');
        pay.css('box-shadow','0px 0px 0px #fff');
        $(this).css('box-shadow','10px 10px 18px #1893D8');
        if (pay.hasClass('pay-checked'))
        {
            pay.removeClass('pay-checked')
        }
        $(this).addClass('pay-checked');
    });


    $(document).ready(function () {
    $('#pay').on('click',function () {
        var pathArray = window.location.pathname.split('/');
        pathArray = pathArray[pathArray.length-1];
        pathArray = parseInt(pathArray);
        if (typeof pathArray === 'number'){

            var sellerId=pathArray;
            var deliveryType=$('.delivery-checked').attr('id');
            var address=$('.address-checked').attr('id');
            var payment=$('.pay-checked').attr('id');
            var cart_id=$('#cart_id').val();


            if( typeof address !== "undefined" && typeof payment !== "undefined" && typeof deliveryType !== "undefined" ){



                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'post',
                    url: '/payment',
                    data: {
                        seller_id: sellerId,
                        cart_id: cart_id,
                        deliveryType: deliveryType,
                        address: address,
                        paymentMethod: payment,

                    },
                    success:function (data) {
                        if (data.status)
                        {
                            if (data.url)
                            {
                             window.open(data.url,'popUpWindow','height=500,width=500,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');
                                window.location.href='https://mahalkum.com';
                            }else{
                                alert(data.message)
                                window.location.href='https://mahalkum.com';
                            }
                        }
                        alert(data.message)
                    }

                }) }else {
                alert("Please Fill All Forms")
            }

        }
    })
    });







