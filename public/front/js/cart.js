$(document).ready(function () {

    $('.deleteSeller').on('click',function () {
        var sellerId =$(this).attr('id')

        var result=confirm('Are you sure you want to delete this item?');
        if (sellerId) {
            if (result) {


                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'delete',
                    url: '/cart/seller/delete/' + sellerId,
                    data: {},
                    success: function (data) {

                        $('.seller' + sellerId).remove()
                    },
                    error: function (data) {

                    }
                })
            }
        }
    })
});