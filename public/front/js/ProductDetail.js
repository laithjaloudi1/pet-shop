$('.singlebuybutton').on('click',function () {
    var product_quantity=$('#product_quantity').val();
    var sellerId =$('#seller_id').val();
    var productId =$('#product_id').val();
    var quantity=$('#quantity').val();
    var product_attribute=[];
    var attr="";
    if ($('.product_details')[0])
    {
        console.log('with combination')
        $('.attributes').each(function () {
            var name =this.className;
            name =name.split(" ");
            product_attribute[this.id]=$('#'+name[1]).val();
            attr +=$('#'+name[1]).val()+"-";
        });
        attr=attr.substring(0,attr.length - 1)

        var data= {
            product_id:productId,
            combination_quantity:quantity,
            combination:attr,
        }
    }else
    {
        var data=
        {
            product_id:productId,
            quantity:quantity,
        }
    }

    if (parseInt(product_quantity) >= parseInt(quantity)){

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'post',
        url: '/cart',
        data: data,
        success:function (data) {
            console.log(data)
            if (!data)
            {
                alert('out of stock or something went wrong')
            }else {
                $('#quantity').val(quantity);
                alert('added to cart successfully');
            }
        },
        error:function (data) {
            console.log(data)
            // alert("please login ");
            // window.location.href='https://mahalkum.com/signin';
        }
    })
}
else {
    alert('quantity is not available')
    }


});
